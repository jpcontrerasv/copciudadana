<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
 

<div class="container mt-5 pt-5">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="col-lg-10">

            <?php the_content(); ?>
            
        </div>

    </div>

</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>