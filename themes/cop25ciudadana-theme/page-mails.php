<?php /* Template Name: Mails */ ?>


<p>Mail de bienvenida de registro</p>
<p>Subject: Bienvenido a la #COP25Ciudadana</p>
<html>

    <head>
        <title>Title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body>

        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">

            <!-- HEADER -->
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

            <tr>
                <td width="600" height="190">
                    <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                </td>
            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                    ¡Gracias [Nombre] por registrarte!
                </td>
            </tr>


            <!--BODY-->
            <tr>

                <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="100" height="50">
                                &nbsp;
                            </td>

                            <td width="400" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">
                                Ahora, junto a ti, le daremos fuerza a 40 medidas Verdes para hacer frente a la actual emergencia climática, presentando estos resultados a los gobiernos del mundo en diciembre, en el marco de la COP 25 Oficial.</td>


                            <td width="100" height="50">
                                &nbsp;
                            </td>


                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td width="600" height="80" style="text-align:center;">

                    <a href="#" style="background: #07BB2C; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.25); border-radius: 8px; font-weight: 700; font-size: 18px; color: #FFFFFF; text-align: center; padding: 12px 36px; cursor: pointer; color#FFF; text-decoration:none;">Crear Evento</a>

                </td>
            </tr>
            <tr>

                <td width="600" height="100" style="text-align:center;">

                    <!--[if mso]>
<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://litmus.com" style="height:36px;v-text-anchor:middle;width:150px;" arcsize="5%" strokecolor="#EB7035" fillcolor="#EB7035">
<w:anchorlock/>
<center style="color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;">I am a button &rarr;</center>
</v:roundrect>
<![endif]-->

                    <a href="#" style="background-color:#07BB2C;border:1px solid #07BB2C;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:240px;-webkit-text-size-adjust:none;mso-hide:all;border-radius:8px;">Participar en un Evento</a>

                </td>

            </tr>

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </table>

                </td>

            </tr>



        </table>

    </body>

</html>

<hr>
<p>Mail de evento aprobado</p>
<p>Subject: #COP25Ciudadana - Tu evento ha sido aprobado</p>
<html>
    <body>
        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">
            <!-- HEADER -->
            <tbody><tr>
                <td width="600" height="50">
                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                            </tr>

                        </tbody></table>

                </td>

                </tr>

                <tr>
                    <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                        #COP25Ciudadana
                    </td>
                </tr>

                <tr>
                    <td width="600" height="190">
                        <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                    </td>
                </tr>

                <tr>
                    <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                        ¡Hola <?php echo $user->first_name;?>!
                    </td>
                </tr>

                <!--BODY-->

                <tr>

                    <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>

                                <tr>

                                    <td width="50" height="100">
                                        &nbsp;
                                    </td>

                                    <td width="500" height="100" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">
                                        <h1>Tu evento ha sido aprobado</h1>
                                        <p>Ahora puedes invitar a quien quieras a participar compartiendo tu evento en redes sociales.</p>
                                    </td>

                                    <td width="50" height="100">
                                        &nbsp;
                                    </td>

                                </tr>

                            </tbody>

                        </table>
                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>

                                    <td width="600" height="20">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>

                                    <td width="100" height="60">
                                        &nbsp;
                                    </td>

                                    <td width="400" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">
                                        <p>No olvides descargar los documentos que te ayudarán a organizar tu COP25 Ciudadana.</p>
                                    </td>

                                    <td width="100" height="60">
                                        &nbsp;
                                    </td>

                                </tr>
                            </tbody>
                        </table>

                        <table width="600" border="0" cellspacing="0" cellpadding="0">

                            <tbody>

                                <tr>

                                    <td width="50" height="34">
                                        &nbsp;
                                    </td>

                                    <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;" >
                                        <p>Instructivo para realizar tu COP</p>
                                    </td>

                                    <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                        <a href="<?php echo get_permalink($post->ID);?>" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/descargar.png" alt="Descarga - COP25 Ciudadana - Partido Ecologista Verde" width="40" height="34" style="display:block; margin:0 auto; border:0px" border="0"></a>
                                    </td>

                                    <td width="50" height="34">
                                        &nbsp;
                                    </td>

                                </tr>

                                <tr>

                                    <td width="50" height="34">
                                        &nbsp;
                                    </td>

                                    <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                        <p>Documento base para iniciar la discusión.</p>
                                    </td>

                                    <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                        <a href="<?php echo get_permalink($post->ID);?>" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/descargar.png" alt="Descarga - COP25 Ciudadana - Partido Ecologista Verde" width="40" height="34" style="display:block; margin:0 auto; border:0px" border="0"></a>
                                    </td>

                                    <td width="50" height="34">
                                        &nbsp;
                                    </td>

                                </tr>

                            </tbody>

                        </table>


                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>

                                    <td width="600" height="20">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>

                                <tr>
                                    <td width="50" height="50">
                                        &nbsp;
                                    </td>

                                    <td width="500" height="50" style="text-align:center;">

                                        <a href="<?php echo get_permalink($post->ID);?>" style="background-color:#07BB2C;border:1px solid #07BB2C;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:240px;-webkit-text-size-adjust:none;mso-hide:all;border-radius:8px;">Ir a mi Evento</a>

                                    </td>


                                    <td width="50" height="50">
                                        &nbsp;
                                    </td>


                                </tr>

                            </tbody>

                        </table>





                    </td>

                </tr>


                <tr>

                    <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td width="100" height="50">
                                    &nbsp;
                                </td>

                                <td width="400" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">
                                    Ahora, junto a ti, le daremos fuerza a las Medidas Verdes para hacer frente a la actual emergencia climática, presentando estos resultados a los gobiernos del mundo en diciembre, en el marco de la COP 25 Oficial.</td>


                                <td width="100" height="50">
                                    &nbsp;
                                </td>


                                </tr>

                            </tbody></table>

                    </td>

                </tr>



                <!--FOOTER-->
                <tr>
                    <td width="600" height="50" style="text-align:center;">
                        Síguenos en nuestras redes

                    </td>
                </tr>
                <tr>

                    <td width="600" height="50">

                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                        <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                                    </td>
                                    <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                        <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                                    </td>
                                    <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                        <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                                    </td>

                                </tr>

                            </tbody>
                        </table>

                    </td>

                </tr>



            </tbody></table>
    </body>
</html>

<hr>
<p>Mail de evento rechazado</p>
<p>Subject: #COP25Ciudadana - Tu evento ha sido rechazado</p>
<html>

    <head>
        <title>Title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body>

        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">

            <!-- HEADER -->
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

            <tr>
                <td width="600" height="190">
                    <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                </td>
            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                    ¡Hola [Nombre]!
                </td>
            </tr>

            <!--BODY-->

            <tr>

                <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50" height="50">
                                &nbsp;
                            </td>

                            <td width="500" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">

                                <h1>Tu evento ha sido rechazado</h1>
                                <p>Tu evento, [nombre del evento], no ha sido aprobado para su publicación. Revisa la información faltante e inténtalo nuevamente. </p>
                                <br>
                                <a href="#" style="background: #07BB2C; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.25); border-radius: 8px; font-weight: 700; font-size: 18px; color: #FFFFFF; text-align: center; padding: 12px 36px; cursor: pointer; color#FFF; text-decoration:none; margin:20px 0;">Organizar un nuevo evento</a>

                            </td>


                            <td width="50" height="50">
                                &nbsp;
                            </td>


                        </tr>

                    </table>

                </td>

            </tr>

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </table>

                </td>

            </tr>



        </table>

    </body>

</html>


<hr>
<p>Mail de participante eliminado</p>
<p>Subject: #COP25Ciudadana - Has sido removido de un evento</p>
<html>

    <head>
        <title>Title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body>

        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">

            <!-- HEADER -->
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

            <tr>
                <td width="600" height="190">
                    <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                </td>
            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                    ¡Hola [Nombre]!
                </td>
            </tr>

            <!--BODY-->

            <tr>

                <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50" height="50">
                                &nbsp;
                            </td>

                            <td width="500" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">

                                <p>Has sido removido del evento [nombre del evento]</p>
                                <br>
                                <a href="#" style="background: #07BB2C; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.25); border-radius: 8px; font-weight: 700; font-size: 18px; color: #FFFFFF; text-align: center; padding: 12px 36px; cursor: pointer; color#FFF; text-decoration:none; margin:20px 0;">Participar en un nuevo evento</a>

                            </td>


                            <td width="50" height="50">
                                &nbsp;
                            </td>


                        </tr>

                    </table>

                </td>

            </tr>

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </table>

                </td>

            </tr>



        </table>

    </body>

</html>

<hr>
<p>Mail de evento eliminado</p>
<p>Subject: #COP25Ciudadana - Evento eliminado</p>
<html>

    <head>
        <title>Title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body>

        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">

            <!-- HEADER -->
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

            <tr>
                <td width="600" height="190">
                    <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                </td>
            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                    ¡Hola [Nombre]!
                </td>
            </tr>

            <!--BODY-->

            <tr>

                <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50" height="50">
                                &nbsp;
                            </td>

                            <td width="500" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">

                                <p>El evento [nombre del evento] ha sido eliminado</p>
                                <br>
                                <a href="#" style="background: #07BB2C; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.25); border-radius: 8px; font-weight: 700; font-size: 18px; color: #FFFFFF; text-align: center; padding: 12px 36px; cursor: pointer; color#FFF; text-decoration:none; margin:20px 0;">Participar en un nuevo evento</a>

                            </td>


                            <td width="50" height="50">
                                &nbsp;
                            </td>


                        </tr>

                    </table>

                </td>

            </tr>

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </table>

                </td>

            </tr>



        </table>

    </body>

</html>



<hr>
<p>Mensaje de Participante a Organizador</p>
<p>Subject: #COP25Ciudadana - Evento eliminado</p>
<html>
    <head>
        <title>Title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body>

        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">

            <!-- HEADER -->
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

<!--BODY-->

            <tr>
                <td width="600" height="80" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; text-align:center;">
                    <h1 style="margin:0; padding:0;">Tienes un nuevo mensaje</h1>
                </td>
            </tr>
            
            <tr>
                <td width="600">
                    
                    <table width="600" border="0" cellspacing="0" cellpadding="0">

                        <tbody>

                            <tr>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                                <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;" >
                                    <p>De:</p>
                                </td>

                                <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                    [Nombres - Apellidos Particpante]
                                </td>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                            </tr>
                            
                            <tr>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                                <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;" >
                                    <p>Email:</p>
                                </td>

                                <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                    <a href="mailto:[Correo participante]">[Correo participante]</a> 
                                </td>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                            </tr>
                            
                            <tr>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                                <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;" >
                                    <p>Teléfono:</p>
                                </td>

                                <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                    [Teléfono Participante]
                                </td>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                            </tr>
                            
                            <tr>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                                <td width="100" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;" >
                                    <p>Mensaje:</p>
                                </td>

                                <td width="400" height="34" style="border:1px solid #FFF; padding-left:10px; background-color:#f0f0f0;">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </td>

                                <td width="50" height="34">
                                    &nbsp;
                                </td>

                            </tr>


                        </tbody>

                    </table>
                </td>
            </tr>

            <tr>
                <td width="600" height="80" style="font-family:Arial, Helvetica, sans-serif; color:#F00; font-weight:bold; vertical-align:middle; mso-line-height-rule:exactly; text-align:center;">
                    <p>No respondas a este correo directamente, <br>hazlo a <a href="mailto:[Correo participante]">[Correo participante]</a> </p>
                </td>
            </tr>

            

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </table>

                </td>

            </tr>



        </table>

    </body>

</html>