
<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>


<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            

<?php
$min=1;
$max=6;
?>

<div id="header-single" class="d-block w-100" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/rand/0<?php echo rand($min,$max);?>.jpg);">

    <?php if(owner_event()) { ?>
    <?php if( get_field('estado') == 'pending' ): ?>
    <div id="alert" class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-10 mt-2">

                <div class="alert alert-warning alert-dismissible" role="alert">
                    <h4 class="alert-heading">¡Felicitaciones!</h4> 
                    <h4 class="alert-heading mb-2">Has creado tu evento y está pendiente de aprobación.</h4>
                    <p>Te notificaremos a tu correo registrado cuando tu evento esté aprobado para que puedas compartirlo en tus redes sociales.</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php } ?>

    &nbsp;
</div>



<div id="single-content" class="container">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="col-lg-6">

            <?php the_content(); ?>
            <?php 
$pais = wp_get_post_terms(get_the_ID(), 'pais');
$pais = implode(', ',wp_list_pluck($pais,'name'));
$commune_term = wp_get_post_terms(get_the_ID(), 'regiones');
$commune = implode(', ',wp_list_pluck($commune_term,'name'));

$location = '';
if(!empty($commune) && !empty($pais)) {
    $auxLocation = [];

    if(!empty($commune)) {
        $auxLocation[] = $commune;
    }

    if(isset($commune_term[0]->parent)) {
        $term = get_term( $commune_term[0]->parent, 'regiones' );
        $auxLocation[] = $term->name;
    }

    if(!empty($pais)) {
        $auxLocation[] = $pais;
    }

    $location =  implode(', ', $auxLocation);
}
            ?> 
            <h3 class="mb-2">
                <?php the_field('direccion');?>
            </h3>
            <h3 class="mb-2">
                <?php if(!empty($location)) { echo "${location}"; } ?>
            </h3>
            <?php if(!is_private_event() ) {?>
            <h3 class="mb-3"><?php echo date_to_es(get_field('fecha_evento'));?></h3>
            <?php } ?>
            <?php if(!owner_event()) { ?>
            <div class="participar">
                <?php if(!is_user_logged_in()) {?>
                <!-- <small class="text-danger">[Si el usuario no está registrado o logueado aparece esto:] </small> -->
                <p><a href="<?php echo site_url(); ?>/registrate/" target="_blank">Regístrate</a> o <a href="#" data-toggle="modal" data-target="#modal-ingresa">Ingresa</a> para participar en este evento</p>
                <?php }  ?>
                <?php $available = count_available_event();?>
                <?php if(!is_participate() && $available > 0 && is_user_logged_in()) {?>
                <div class="d-block border-top pt-3 pb-3 mt-3 mb-3">
                    <br>
                    <a href="#" data-toggle="modal" data-target="#modal-participar" class="cta">Participar</a>
                    <p>Quedan <strong><?php echo count_available_event();?></strong> cupos</p>
                </div>
                <?php } ?>
                <?php if(!is_user_logged_in() && !is_participate() ){?>
                <div class="d-block border-top pt-3 pb-3 mt-3 mb-3">
                    <br>
                    <a href="#" data-toggle="modal" data-target="#modal-participar" class="cta">Participar</a>
                </div>
                <?php }?>
                <?php if(is_participate() && is_user_logged_in()) {?>   
                <div class="d-block border-top pt-3 pb-3 mt-3 mb-3">
                    <form rel="message">
                        <div class="alert" style="display:none;" rel="notice"></div>
                        <div class="form-group">
                            <label>Envía un mensaje al organizador del evento</label>
                            <p>En el mensaje enviado incluiremos tu correo y tu nombre registrados para que el organizador pueda responderte directamente.</p>
                            <textarea rows="5" name="message" required></textarea>
                        </div>
                        <input type="hidden" name="post_id" value="<?php the_ID();?>">
                        <input type="submit" value="Enviar mensaje">
                    </form>


                </div>

                <?php } ?>
            </div>
            <?php } ?>

        </div>
        <div class="col-lg-6">

            <?php if( get_field('estado') == 'pending' ): ?>

            <?php else: // field_name returned false ?>
            <div class="d-block mb-4">
                <p class="mb-3">Compartir:</p>
                <div class="addthis_inline_share_toolbox_t5ko"></div>

            </div>

            <?php endif; ?>

            <?php if(is_participate() && is_user_logged_in()) {?>   

            <div class="d-block border-top pt-3 pb-3 mt-3 mb-3 text-right">
                <a href="#" data-toggle="modal" data-target="#modal-no-participar" class="text-danger">Dejar de participar en este evento</a>
            </div>
            
            <?php } ?>


        </div>

        <!--Lista de participantes-->
        <div id="participantes" class="col-lg-12 mt-5 mb-5 pb-5">

            <h1 class="mb-3">Lista de participantes</h1>
            <?php if(owner_event()) {?>
            <p>Esta es la lista de participantes de tu evento. Cuando alguien se registre aparecerá acá.</p>
            <?php } ?>

            <table
                   data-toggle="table"
                   data-click-to-select="true"
                   data-pagination="true"
                   data-search="true"
                   data-page-size="50"
                   data-pagination-loop="false"
                   id="participants"
                   >
                <thead>
                    <tr>
                        <?php if(owner_event()) {?>
                        <th data-field="state" data-checkbox="true"></th>
                        <?php } ?>
                        <th data-field="id">Nº</th>
                        <th data-field="nombre">Nombre</th>
                        <th data-field="origen">Desde</th>
                        <?php if(owner_event()) {?>
                        <th data-field="administrar">Administrar</th>
                        <?php } ?>
                    </tr>
                </thead>
                <?php $participantes = get_field('participantes');?>

                <tbody>
                    <?php $count = 1;?>
                    <?php foreach($participantes as $participante) {?>
                    <?php 
                $country = get_user_meta($participante["ID"], 'country', true);
                $regions = get_user_meta($participante["ID"], 'regions', true);
                $commune = get_user_meta($participante["ID"], 'commune', true);
                $location = 'No existe información';
                if(!empty($country) || !empty($regions) || !empty($commune)) {
                    $auxLocation = [];
                    if(!empty($country)) {
                        $auxLocation[] = $country;
                    }
                    if(strtolower($country) == 'chile') {
                        if(!empty($regions)) {
                            $auxLocation[] = $regions;
                        }
                        if(!empty($commune)) {
                            $auxLocation[] = $commune;
                        }
                    }
                    $location = implode(', ', $auxLocation);
                }
                    ?>
                    <tr>
                        <?php if(owner_event()) {?>
                        <td>&nbsp;</td>
                        <?php } ?>
                        <td><?php echo $count;?></td>
                        <td><?php echo $participante["user_firstname"]." ".$participante["user_lastname"];?></td>
                        <td><?php echo $location?>
                            <?php if(owner_event()) {?>
                        <td><button type="button" class="btn btn-danger" data-index="<?php echo $count;?>" data-action="delete" data-event="<?php the_ID();?>" data-id="<?php echo $participante['ID'];?>">Eliminar</button></td>
                        <?php } ?>
                    </tr>
                    <?php $count++;?>
                    <?php } ?>

                </tbody>
            </table>

        </div>


        <?php if(!is_open_event()) {?>
        <!--Conclusiones-->
        <div id="conclusiones" class="col-lg-12 mb-5 pb-5">
            <h1 class="mb-3">Acta COP25Ciudadana</h1>
            <p class="mb-4">En el evento <?php the_title(); ?>, realizado con día y hora <?php echo date_to_es(get_field('fecha_evento'));?> y reunidos <?php the_field('cantidad_de_asistentes');?> asistentes, acordamos las siguientes conclusiones de nuestra COP25 Ciudadana.</p>
            <p class="mb-2">Respecto de las propuestas para políticas públicas para los gobiernos, parlamentos y demás instituciones de los Estados, nosotros suscribimos: </p>
            <blockquote><?php the_field('acta_estado');?></blockquote>
            <p class="mb-2">Respecto de las propuestas de cambios de costumbres y hábitos de consumo que deberíamos asumir como ciudadanía, hemos acordado:</p>
            <blockquote><?php the_field('acta_ciudad');?></blockquote>

        </div>
        <?php } ?>

    </div>

</div>

<!--Area privada-->
<?php if(owner_event()) { ?>
<div id="area-privada" class="container mb-5">

    <div class="row">
        <div class="col-lg-12 mt-4">
            <h1 class="font-weight-bold">Configuración de tu evento</h1>
            <p class="mb-5">Esta área es visible sólo para ti</p>
        </div>
    </div>

    <div class="row">

        <!--Descargar Documentos-->
        <div class="col-lg-6 mb-4">
            <h2 class="mb-4">Descargar Documentos</h2>

            <table class="table mb-4" style="height:auto;">
                <thead>
                    <tr>
                        <th scope="col">Documento</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Instructivo para realizar tu COP</td>
                        <td><small>El instructivo es la guía oficial para llevar a cabo tu COP.</small></td>

                        <td><a href="<?php echo get_template_directory_uri(); ?>/Instructivo-cop25-ciudadana.pdf" download="<?php echo get_template_directory_uri(); ?>/Instructivo-cop25-ciudadana.pdf"><i class="fa fa-download  text-ciudadano" aria-hidden="true"></i></a></td>
                    </tr>

                    <tr>
                        <td>Documento base para iniciar la discusión.</td>
                        <td><small>Utiliza este documento para iniciar una conversación en tu COP Ciudadana.</small></td>

                        <td><a href="<?php echo get_template_directory_uri(); ?>/Instructivo-cop25-ciudadana.pdf" download="<?php echo get_template_directory_uri(); ?>/Instructivo-cop25-ciudadana.pdf"><i class="fa fa-download  text-ciudadano" aria-hidden="true"></i></a></td>
                    </tr>


                    <tr>
                        <td>40 Medidas Verdes.</td>
                        <td><small>Utiliza estas propuestas de Medidas Verdes para incluirlas en tu conversación.</small></td>

                        <td><a href="<?php echo get_template_directory_uri(); ?>/img/40-Medidas-Verdes.pdf" download="<?php echo get_template_directory_uri(); ?>/img/40-Medidas-Verdes.pdf"><i class="fa fa-download  text-ciudadano" aria-hidden="true"></i></a></td>
                    </tr>



                </tbody>
            </table>


            <!-- Afiche -->
            <div class="d-block border-top pt-3 pb-3 mt-3 mb-3">
                <h2 class="mb-2">Descarga tu afiche</h2>
                <p class="mb-2">Hemos diseñado un afiche para que puedas difundir tu evento</p>
                <div class="d-block mb-2">
                    <a href="<?php echo home_url('?action=wp_event_poster&id='.get_the_ID()); ?>" download="afiche.png" ><img src="<?php echo home_url('?action=wp_event_poster&id='.get_the_ID()); ?>" alt=""></a>
                </div>
                <a href="<?php echo home_url('?action=wp_event_poster&id='.get_the_ID()); ?>" class="cta small-cta fright" download="afiche.png" >Descargar tu afiche <i class="fa fa-download" aria-hidden="true"></i></a>           
            </div>


        </div>

        <!--Configuración del Evento-->
        <div class="col-lg-6 mb-4">

            <h2 class="mb-4">Configuración del evento</h2>
            <form rel="event" method="POST">
                <div class="alert" style="display:none;" rel="notice"></div>
                <div class="form-group row">
                    <label class="col-5 col-form-label">Privacidad del Evento</label>
                    <div class="col-7">
                        <?php $tipo = get_field('tipo');?>
                        <input type="checkbox" value="public" name="type" <?php if($tipo === 'public') { ?>checked <?php } ?> data-toggle="toggle" data-on="Publico" data-off="Privado" data-onstyle="success" data-offstyle="danger">
                    </div>
                </div>
                <?php 
                         $date = get_field('fecha_evento');
                         $fecha = explode(' ', $date);
                ?>
                <div class="form-group row">
                    <label for="staticEmail" class="col-5 col-form-label">Dirección del evento</label>
                    <div class="col-7">
                        <input type="text"  name="address" value="<?php the_field('direccion');?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-5 col-form-label">Fecha del evento</label>
                    <div class="col-7">
                        <input type="text" name="date" id="pickdate" value="<?php echo @$fecha[0];?>">
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-5 col-form-label">Hora del Evento</label>
                    <div class="col-7">
                        <input type="text" name="hour" id="picktime" value="<?php echo @$fecha[1].' '.strtoupper(@$fecha[2]);?>">
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-5 col-form-label">Límite de asistentes</label>
                    <div class="col-7">
                        <input type="number" name="limit" value="<?php the_field('limite_de_asistentes');?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-5">
                        <p>
                            <a href="#" data-toggle="modal" data-target="#modal-eliminar"  class="text-danger">¿Deseas eliminar este evento?</a> 
                        </p>
                    </div>
                    <div class="col-7 text-right">
                        <input type="submit" class="small-cta" value="Guardar cambios">
                    </div>
                </div>
                <input type="hidden" name="post_id" value="<?php the_ID();?>">
            </form>



        </div>


        <?php if(!is_open_event()) {?>
        <!--Acta de Conclusiones-->

        <div class="col-12 border-top mb-4 pb-3 pt-5">

            <div class="row">

                <div class="col-12 mb-4 text-center">
                    <h2 class="mb-4">Acta de Conclusiones</h2>
                </div>

                <div class="col-lg-4">



                    <p class="mb-2">Luego de realizar tu evento, el día <?php echo date_to_es(get_field('fecha_evento'));?>, <strong>debes declarar tu acta de conclusiones acá</strong>. Esta acta será pública una vez envíes tus conclusiones. </p>

                    <p class="mb-2">Con todas las actas recibidas hasta el 30 de septiembre se elaborará un documento final con todas las materias propuestas, respecto de las cuales se propondrán distintas alternativas para enfrentarlas.</p>

                    <p class="mb-2">Ya hiciste un evento de la Cop 25 Ciudadana y los asistentes participaron en la elaboración del documento final a votar. Si gustas, puedes hacer otro evento después del 30 de septiembre para revisar junto a los asistentes las propuestas antes de votar. En esa etapa se puede sumar más personas al registro.</p>

                    <p class="mb-2">Todas las personas que hayan participado y se hayan registrado en www.cop25ciudadana.cl ya sea antes o con posterioridad al 30 de septiembre, podrán votar por las diferentes opciones para enfrentar cada una de las materias.</p>

                    <p class="mb-2">Finalmenente, entregaremos los resultados a la ONU y a la Unión Interparlamentaria, así como también, difundiremos las medidas ciudadanas para ponerlas en práctica.</p>

                </div>

                <div class="col-lg-8">

                    <form class="pb-5 mb-5" rel="act" method="POST">
                        <div class="alert" style="display:none;" rel="notice"></div>
                        <!--<small class="text-danger">[Este formulario sólo se habilita para después de la fecha del evento. Dejé desactivado el botón de submit, que se activa sólo después de la fecha.]</small>-->

                        <div class="form-group row">
                            <label class="col-5 col-form-label">Identificación de la institución, organización o grupo</label>
                            <div class="col-7">
                                <input type="text" value="<?php the_title(); ?>" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-5 col-form-label">Día y lugar en que se realizó</label>
                            <div class="col-7">
                                <input type="text" value="<?php echo date_to_es(get_field('fecha_evento'));?>, <?php the_field('direccion');?>" disabled>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-5 col-form-label">Cantidad de Asistentes</label>
                            <div class="col-7">
                                <input type=number value="<?php the_field('cantidad_de_asistentes');?>" name="assistants" required>
                            </div>
                        </div>

                        <div class="form-control bg-transparent">
                            <label>Respecto de las propuestas para políticas públicas para los gobiernos, parlamentos y demás instituciones de los Estados: </label>
                            <div>
                                <textarea rows="3" name="act_state" required><?php the_field('acta_estado');?></textarea>
                                <small>Te recomendamos producir un texto breve y con puntos específicos.</small>
                            </div>
                        </div>

                        <div class="form-control bg-transparent">
                            <label>Respecto de las propuestas de cambios de costumbres y hábitos de consumo que deberíamos asumir como ciudadanía.</label>
                            <div>
                                <textarea rows="3" name="act_city" required><?php the_field('acta_ciudad');?></textarea>
                                <small>Te recomendamos producir un texto breve y con puntos específicos.</small>
                            </div>
                        </div>


                        <div class="form-control text-right bg-transparent">
                            <input type="submit" class="small-cta" value="Enviar Conclusiones" >
                            <br>
                            <small>Podrás enviar tu información después del <?php echo date_to_es(get_field('fecha_evento'));?> </small>
                            <input type="hidden" name="post_id" value="<?php the_ID();?>">
                        </div>


                    </form>

                </div>

            </div>
            <?php } ?>

        </div>

    </div>

</div>
<?php } ?>


<!--modal Participar-->
<div class="modal fade" id="modal-participar" tabindex="-1" role="dialog" aria-labelledby="modal-participarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Participar en <?php the_title(); ?> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <?php
if ( is_user_logged_in() ) { ?>

            <div class="modal-body">
                <div class="alert" rel="notice-join" style="display:none"></div>
                Al presionar <strong>Confirmar</strong> estarás participando en el evento <strong><?php the_title(); ?></strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-verde" data-action="join" data-id="<?php the_ID();?>" >Confirmar</button>
            </div>


            <?php } else { ?>

            <div class="modal-body">
                <div class="alert" rel="notice-join" style="display:none"></div>
                Para participar de este evento debes <a href="<?php echo site_url(); ?>/registrate/" target="_blank"> registrarte</a>. 
            </div>
            <div class="modal-footer">
                &nbsp;
            </div>





            <?php } ?>





        </div>
    </div>
</div>


<!--modal Dejar de Participar-->
<div class="modal fade" id="modal-no-participar" tabindex="-1" role="dialog" aria-labelledby="modal-no-participarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Dejar de participar en <?php the_title(); ?> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert" rel="notice-join" style="display:none"></div>
                Al presionar confirmar dejarás de participar en el evento <strong><?php the_title(); ?></strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-verde" data-action="quit" data-id="<?php the_ID();?>">Confirmar</button>
            </div>
        </div>
    </div>
</div>


<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>




<!--modal Ingresar-->
<div class="modal fade" id="modal-ingresa" tabindex="-1" role="dialog" aria-labelledby="modal-ingresaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ingresar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-5 col-form-label">Email</label>
                        <div class="col-7">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-5 col-form-label">Password</label>
                        <div class="col-7">
                            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary btn-verde">Ingresar</button>
                        </div>
                    </div>



                </form>




            </div>
            <div class="modal-footer text-left">
                <a href="<?php echo site_url(); ?>/wp-login.php?action=lostpassword" target="_blank">Olvidé mi contraseña</a> 
            </div>

        </div>
    </div>
</div>

<!--modal Elminar-->
<div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Deseas elminar este evento?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Los participantes serán notificados de su eliminación.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-action="delete" data-id="<?php the_ID();?>">Eliminar</button>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?>
