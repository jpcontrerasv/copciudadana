<?php /* Template Name: Participa */ ?>
<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            


<div class="container">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5 mt-5">
            <h1 class="mb-3"><?php the_title(); ?></h1>
            <p>Acá podrás ver todos los eventos de COP Ciudadana inscritos hasta ahora a través de Chile y el mundo.</p>
        </div>

        <div class="col-12">
            <table
                   rel="table-event"
                   data-toggle="table"
                   data-pagination="true"
                   data-page-size="20"
                   data-search="true"
                   data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=wp_event_all_ajax" class="table-striped">
                
                <thead class="thead-dark">
                    <tr>
                        <th data-field="title" data-sortable="true">Nombre del Organizaodr</th>
                        <th data-field="country" data-sortable="true">País</th>
                        <th data-field="regions" data-sortable="true">Región</th>
                        <th data-field="commune" data-sortable="true">Comuna</th>
                        <th data-field="link" data-formatter="tableEventList.formatLink">Link</th>
                    </tr>
                </thead>
            </table>
            <script> 
                var tableEventList = new Object();
                tableEventList.formatLink = function(value, row) {
                    return `<a href="${value}">Ver Evento</a>`;
                }
            </script>
        </div>

    </div>

</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>