<?php redirect_private_page(['administrator', 'participante', 'organizador']);?>
<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>

<div class="container mt-5">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="col-lg-12">

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">

                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Mi Perfil</a>

                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Eventos en los que participo</a>

                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Eventos que organizo</a>

                </div>

            </nav>

            <div class="tab-content" id="nav-tabContent">


                <!--- TAB 1 --->
                <div class="tab-pane fade show active pt-4" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                    <p class="mb-3">Puedes editar tu perfil desde acá</p>
                    <?php echo do_shortcode('[form_perfil]');?>

                    <div class="d-block border-top pt-3 pb-3 mt-3 mb-3 text-right">
                        <?php echo do_shortcode( '[plugin_delete_me /]' ); ?>
                    </div>

                </div>
                <!--- FIN TAB 1 --->


                <!--- TAB 2 --->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <?php echo do_shortcode('[form_perfil_participante]');?>
                </div>

                <!--- TAB 3 EVENTOS QUE ORGANIZO --->
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <?php echo do_shortcode('[form_perfil_eventos]');?>
                </div>
            </div>


        </div>

    </div>

</div>

<?php get_footer(); ?>