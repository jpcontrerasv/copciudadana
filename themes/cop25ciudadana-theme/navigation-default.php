<?php
$main_nav_options = array(
    'theme_location'    => 'main_menu',
    'depth'             => 2,
    'container'         => '',
    'container_class'   => '',
    'menu_class'        => 'nav navbar-nav',
    'fallback_cb'       => 'bootstrap_four_wp_navwalker::fallback',
    'walker'            => new bootstrap_four_wp_navwalker()
);
?>
<div id="header">
    <header class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 logo d-flex align-items-center justify-content-between">
                <a href="<?php echo site_url(); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/img/logo-PEV.png" alt=""></a>

                <?php if ( is_user_logged_in() ) { ?>
                <p class="user-link">
                    <a href="<?php echo site_url(); ?>/mi-perfil">Mi Perfil</a>
                    |
                    <a class="text-danger" href="<?php echo site_url(); ?>/wp-login.php?action=logout">Cerrar Sesión</a>
                </p>
                <?php } else { ?>
                <p class="user-link">
                    <a href="#" data-toggle="modal" data-target="#modal-ingresa">Iniciar Sesión</a>
                </p>
                
                <?php } ?>


            </div>

            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 d-flex align-items-center">

                <ul class="d-flex justify-content-around fwidth">
                    <li class="d-flex align-items-center">
                        <h1 class="d-inline">
                            <a href="<?php echo site_url(); ?>">#COP25Ciudadana</a> 
                        </h1>
                    </li>
                    <li class="d-flex align-items-center">
                        <a href="https://twitter.com/ecologistachile" target="_blank">
                            <i class="fa fa-twitter fa-3x" aria-hidden="true"></i>
                        </a> 
                    </li>
                    <li class="d-flex align-items-center">
                        <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank">
                            <i class="fa fa-facebook fa-3x" aria-hidden="true"></i>
                        </a> 
                    </li>

                    <li class="d-flex align-items-center">
                        <a href="https://www.instagram.com/partidoecologistaverde" target="_blank">
                            <i class="fa fa-instagram fa-3x" aria-hidden="true"></i>
                        </a> 
                    </li>
                </ul>

            </div>

            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 d-flex  align-items-center justify-content-center text-center profile-link">


                <?php  if ( is_user_logged_in() ) { ?>
                <p>
                    <a class="d-flex flex-column" href="<?php echo site_url(); ?>/mi-perfil">
                        <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                        <small>Mi Perfil</small>
                    </a>
                    <a href="<?php echo site_url(); ?>/wp-login.php?action=logout" class="text-danger"><small>Cerrar sesión</small></a> 
                </p>
                <?php } else { ?>

                <p>
                    <a href="#" data-toggle="modal" data-target="#modal-ingresa" class="d-flex flex-column">
                        <i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>
                        <small>Ingresar</small>
                    </a>
                </p>
                <?php } ?>



            </div>
        </div>
    </header>

    <nav id="sub-header" class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="col-12 p0 d-flex">
                    <ul class="list-inline">
                        
                        <?php if( current_user_can('administrator') ) {  ?>
                        <li>
                            <a href="<?php echo site_url(); ?>/administracion-de-eventos/">Administrar eventos</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>/blog/">Blog</a>
                        </li>
                        

                        <?php } else { ?>
                        <?php 
                        $my_menu = array( 
                            'menu' => 'main_menu',
                            'container' => '',
                            'items_wrap' => '%3$s' 
                        );
                        wp_nav_menu( $my_menu );
                        ?>
               <?php } ?>         
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>


<!--modal Ingresar-->
<div class="modal fade" id="modal-ingresa" tabindex="-1" role="dialog" aria-labelledby="modal-ingresaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ingresar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form rel="login" method="post" autocomplete=off>
                    <div rel="notice-login" class="alert" style="display:none;"></div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-5 col-form-label">Email</label>
                        <div class="col-7">
                            <input type="text" class="form-control-plaintext" name="email" id="staticEmail" placeholder="nombre@correo.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-5 col-form-label">Contraseña</label>
                        <div class="col-7">
                            <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Contraseña">
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary btn-verde" data-action="login">Ingresar</button>
                        </div>
                    </div>



                </form>




            </div>
            <div class="modal-footer text-left">
                <a href="<?php echo site_url(); ?>/registrate">Registrarse</a> 
                &nbsp;|&nbsp;                
                <a href="<?php echo site_url(); ?>/wp-login.php?action=lostpassword" target="_blank">Olvidé mi contraseña</a> 
            </div>

        </div>
    </div>
</div>