
<?php global $post; ?>
<div class="d-block w-100 mt-5 mb-5 p-1 border-2">
&nbsp;
</div>

<?php if( $post->ID == 6 || $post->ID == 8) { ?>



<?php } else { ?>
<section class="container mt-5">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-left">
            <h2>Descarga el Kit de Prensa</h2>
            <p>Descarga el comunicado de prensa, logos e imágenes.</p>
            <br>
            <a href="<?php the_field('kit_de_prensa', 'option'); ?>" class="cta small-cta" download="<?php the_field('kit_de_prensa', 'option'); ?>" >Descargar</a>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-left">
            <h2>Suscríbete a las novedades</h2>

            <!-- Begin Mailchimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="https://cop25ciudadana.us3.list-manage.com/subscribe/post?u=ce8bfa5415ccaab3aa17c5ef4&amp;id=d9168897e2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">

                        <div class="indicates-required">
                            <p>Déjanos tu email y te contaremos de todas las novedades sobre la COP25 Ciudadana.</p>
                        </div>
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                            <small>Sólo usaremos tu email para contarte de nuestras novedades y nunca lo entregaremos a terceros.</small>
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ce8bfa5415ccaab3aa17c5ef4_d9168897e2" tabindex="-1" value=""></div>
                        <div class="clear">
                            <input type="submit" value="Subscríbete" name="subscribe" id="mc-embedded-subscribe" class="cta"></div>
                    </div>
                </form>
            </div>

            <!--End mc_embed_signup-->


        </div>


    </div>
</section>

<?php } ?>


<footer class="d-flex justify-content-center align-items-end w-100 pb-4 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12 text-center mb-5">
                <img src="<?php echo get_template_directory_uri();?>/img/logo-PEV.png" alt="">
            </div>
            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-12 col-12">
                <ul>
                    <li><a href="<?php echo site_url(); ?>/manifiesto">Manifiesto</a></li>
                    <li><a href="https://www.ecologistas.cl/" target="_blank">Ecologistas.cl</a></li>
                    <li><a href="<?php echo site_url(); ?>/crea-tu-cop-ciudadana/">Organiza tu COP Ciudadana</a></li>
                    <li><a href="<?php echo site_url(); ?>/participa">Participa en una COP Ciudadana</a></li>
                </ul>
            </div>
        </div>
    </div>

</footer>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.rut.v2.js"></script>	

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.sticky-kit.min.js"></script>	

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/picker.js"></script>	

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/picker.date.js"></script>	

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/picker.time.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.validate.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/additional-methods.js"></script>


<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/localization/messages_es.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/bootstrap-table.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.2/locale/bootstrap-table-es-CL.js"></script>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=tarkadd"></script>


<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/custom.js"></script>		

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143526081-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143526081-1');
</script>


</body>

</html>
