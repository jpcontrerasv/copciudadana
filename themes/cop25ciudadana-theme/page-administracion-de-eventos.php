<?php /* Template Name: Administracion de eventos */ ?>
<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            


<div class="container">

    <div class="row d-flex justify-content-center">
        <div class="col-lg-12 mb-5 mt-5">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="col-lg-12">
            <div class="d-block w-100 text-right fleft pb-3">
                <a href="<?php echo home_url('?action=wp_download_event_excel&v='.rand(0, 1000));?>" class="btn btn-primary">Exportar  a excel <i class="fa fa-download" aria-hidden="true"></i></a>
            </div>
          
            <table
                rel="admin-manager"
                data-toggle="table"
                data-pagination="true"
                data-page-size="20"
                   data-search="true"
                data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=wp_event_ajax" class="table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th data-field="title" data-sortable="true">Nombre del evento</th>
                        <th data-field="detail">Detalles</th>
                        <th data-field="status" data-formatter="tableManager.formattStatus" data-sortable="true">Estado</th>
                        <th data-field="link" data-formatter="tableManager.formattLink">Enlace</th>
                        <th data-field="action" data-formatter="tableManager.formattAction">Acciones</th>
                    </tr>
                </thead>
            </table>
            <script> 
                var tableManager = new Object();
                tableManager.formattStatus = function(value) {
                    if(value === 'publish') {
                        return '<p class="text-success">Aprobado</p>';
                    } 
                    if(value === 'pending') {
                        return '<p class="text-warning">Pendiente</p>';
                    }
                    return '<p class="text-danger">Rechazado</p>';
                }
                
                tableManager.formattAction = function(value, row) {
                   if(value=== 'pending') {
                       return `
                        <button type="button" class="btn btn-sm btn-warning" data-id="${row.id}" data-action="reject">Rechazar</button>
                        <button type="button" class="btn  btn-sm btn-success" data-id="${row.id}" data-action="approve">Aprobar</button>
                        `;
                   }
                   return `<button type="button" class="btn btn-sm btn-danger" data-id="${row.id}" data-action="delete">Eliminar</button>`;
                }
                tableManager.formattLink = function(value, row) {
                    return `<a href="${value}" target="_blank">Ir a Evento</a>`;
                }
            </script>
        </div>

    </div>

</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>