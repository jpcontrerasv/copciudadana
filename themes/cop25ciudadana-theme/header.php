<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="content-type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
        <meta name="description" content="<?php echo esc_attr( get_bloginfo( 'description' ) ); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
              
<?php if ( is_singular('eventos') ) : ?>
      
      <!--eventos-->
        <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>        

        <!--meta regular-->
        <meta name="title" content="Evento COP25 Ciudadana: <?php the_title(); ?>"/>
        <meta name="description" content="Participa en la COP25 Ciudadana de <?php the_title(); ?>. <?php echo date_to_es(get_field('fecha_evento'));?> <?php the_field('direccion');?>"/>
        <!--open graph facebook-->
        <meta property="og:url" content="<?php the_permalink(); ?>" />
        <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/rand/03.jpg" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Evento COP25 Ciudadana: <?php the_title(); ?>" />
        <meta property="og:description" content="Participa en la COP25 Ciudadana de <?php the_title(); ?>. <?php echo date_to_es(get_field('fecha_evento'));?> <?php the_field('direccion');?>" />
        <meta property="og:site_name" content="COP 25 Ciudadana - Partido Ecologista Verde - Inscríbete - Participa - Actúa"/>

        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>

<?php elseif( is_singular('news') ): ?>
      
<?php else : ?>
      
        <meta property="og:url" content="https://cop25ciudadana.cl" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="COP25 Ciudadana - Partido Ecologista Verde - Inscríbete, Participa, Actúa" />
        <meta property="og:description" content="Te invitamos a que acordemos 40 medidas Verdes, entre políticas públicas y compromisos ciudadanos, para hacer frente a la actual emergencia climática. Con tu participación y la de muchas otras personas en esta plataforma le daremos fuerza a estas 40 medidas, que serán entregadas a los gobiernos del mundo, en diciembre, en el marco de la COP 25 Oficial." />
        <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/rand/03.jpg" />       
  
<?php endif; ?>
        
        
        
        
        <?php wp_head(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/custom.css">

        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/default.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/default.date.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/default.time.css">

        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

        <link href="https://unpkg.com/bootstrap-table@1.15.2/dist/bootstrap-table.min.css" rel="stylesheet">


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    </head>
    <body <?php body_class(); ?>>
