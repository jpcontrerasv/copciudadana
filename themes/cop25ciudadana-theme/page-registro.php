<?php /* Template Name: Registro */ ?>
<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>

<div class="container">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5 mt-5">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="col-lg-12">

            <p class="mb-5">Crea tu cuenta para que puedas registrar COP Ciudadanas y/o Participar en las que existen.</p>

            <?php echo do_shortcode('[form_registrate]');?>

        </div>



    </div>

</div>

<?php get_footer(); ?>
