<?php get_header(); ?>
<?php get_template_part( 'navigation', 'default' ); ?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            

<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

<!--IMAGEN ENCABEZADO-->
<?php if ( has_post_thumbnail() ) { ?>
<div class="container mb-5">
    <div class="row">
        <div class="col-12 hero-img bg-cover position-relative mb-50" style="background-image:url(<?=$url?>);">
            &nbsp;
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="container-fluid mb-5">
    <div class="row">
        <div class="col-12 position-relative mb-5">
            &nbsp;
        </div>
    </div>
</div>
<?php  }   ?> 

<!--CONTENIDO PRINCIPAL-->
<div class="container-fluid pb-5">

    <div id="content-single" class="container">
        <div class="row">
            <div class="col-12 mb-4 text-center">
                <h1 class="mb-3"><?php the_title(); ?></h1>
                <p class="m-0">Fecha de publicación: <?php the_time('j'); ?>/<?php the_time('m'); ?>/<?php the_time('y'); ?> a las <?php the_time('H'); ?>:<?php the_time('i'); ?>. <br>
                    Tags: <?php $post_tags = get_the_tags();
 
if ( $post_tags ) {
    foreach( $post_tags as $tag ) {
    echo $tag->name . ', '; 
    }
} ?></p>

                <p><small>Última actualización: <?php the_modified_date('j/m/y'); ?> a las <?php the_modified_date('H:i'); ?></small></p>


            </div>

            <div id="section-add-this" class="col-12 border-top border-bottom pt-3 pb-3 text-center mb-5">
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_t5ko"></div>
            </div>


            <div class="col-10 offset-1">
                <?php if( is_single( 274 ) ) { ?>
                <h3><strong>Actualizado el <?php the_modified_date('j'); ?> de <?php the_modified_date('F'); ?> a las <?php the_modified_date('H:i'); ?> hrs.</strong></h3>
                <?php } else { ?>
                <?php } ?>
                <?php the_content(); ?>
            </div>
            <!--/content single -->

        </div>
    </div>

</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php wp_reset_query(); ?>


<?php get_footer(); ?>
