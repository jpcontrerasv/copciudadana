<?php get_header(); ?>
<?php get_template_part( 'navigation', 'default' ); ?>



<?php if ( has_post_thumbnail() ) { ?>
<!--IMAGEN ENCABEZADO-->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 hero-img bg-cover position-relative mb-50" style="background-image:url(<?php the_post_thumbnail_url('full'); ?>);">
            &nbsp;
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 position-relative mb-5">
            &nbsp;
        </div>
    </div>
</div>
<?php  }   ?> 

<!--CONTENIDO PRINCIPAL-->
<div class="container-fluid mt-5 mb-5">

    <div id="content-single" class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1><?php printf( __( 'Todo sobre: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>
            </div>
        </div>
    </div>
</div>

<div class="container mb-50">
    
    
    
    
    <div class="row">
      <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
 
        
        <div class="card col-lg-4 mb-4 border-0">
            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full'); ?>" class="card-img-top" alt="..."></a>
            <div class="card-body">
                <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
                <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="<?php the_permalink(); ?>" class="btn cta smallest-cta fright">Leer noticia</a>
            </div>
        </div>
        
         <?php endwhile; ?>
        
       <?php /*
        <nav class="w-100 d-flex justify-content-center mb-20" >
            <?php wp_pagenavi(); ?>
        </nav>
        */?>

        <?php else : ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        
    </div>
    
</div>

<?php get_footer(); ?>