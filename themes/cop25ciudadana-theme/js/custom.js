jQuery(document).ready(function ($) {

    //RUT
    $("#rut").Rut({
        on_error: function(){ 
            swal('El RUT ingresado no es un RUT válido de Chile o el formato no es el correcto.');
            $('#rut').addClass("error");
        },
        on_success: function(){ 
            $('#rut').removeClass("error");
        }

    });

    //STICK IN PARENT
    $("#header").stick_in_parent({});

    $("#section-add-this").stick_in_parent({
        parent: 'content-single'
    });

    $("#alert").stick_in_parent({
        offset_top: 170,
        parent:'body'
    });

    //DATEPICKERS
    $('#pickdate').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        today: 'Hoy',
        format: 'dd/mm/yyyy',
        formatSubmit: 'dd/mm/yyyy',
        clear: 'Borrar',
        close: 'Cerrar',
        firstDay: 2,
        min:0,
        max: [2019,11,1]
    });

    $('#fecha-de-nacimiento').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        today: 'Hoy',
        clear: 'Borrar',
        close: 'Cerrar',
        firstDay: 2,
        labelMonthNext: 'Go to the next month',
        labelMonthPrev: 'Go to the previous month',
        labelMonthSelect: 'Pick a month from the dropdown',
        labelYearSelect: 'Pick a year from the dropdown',
        format: 'dd/mm/yyyy',
        formatSubmit: 'dd/mm/yyyy',
        selectMonths: true,
        selectYears: true,
        selectYears: 100,
        max:0

    });

    $('#picktime').pickatime({
        clear: 'Borrar',
        interval: 30
    });





    $(".validar").validate({
        onsubmit: true,
        rules: {
            aceptartyc: {
                required: true
            }
        }
    });


    jQuery(document).ready(function () {
        var iRegion = 0;
        var htmlRegion = '<option value="sin-region">Seleccione Región</option><option value="sin-region" disabled>-----------</option>';
        var htmlComunas = '<option value="sin-region">Seleccione Comuna</option><option value="sin-region" disabled>-----------</option>';

        jQuery.each(RegionesYcomunas.regiones, function () {
            htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + 
                RegionesYcomunas.regiones[iRegion].NombreRegion +
                '</option>';
            iRegion++;
        });

        jQuery('#regiones').html(htmlRegion);
        jQuery('#comunas').html(htmlComunas);

        jQuery('#regiones').change(function () {
            var iRegiones = 0;
            var valorRegion = jQuery(this).val();
            var htmlComuna = '<option value="sin-comuna">Seleccione Comuna</option><option value="sin-comuna" disabled>-----------</option>';
            jQuery.each(RegionesYcomunas.regiones, function () {
                if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
                    var iComunas = 0;
                    jQuery.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
                        htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '" >' +
                            RegionesYcomunas.regiones[iRegiones].comunas[iComunas] +
                            '</option>';
                        iComunas++;
                    });
                }
                iRegiones++;
            });
            jQuery('#comunas').html(htmlComuna);
        });
        jQuery('#comunas').change(function () {
            if (jQuery(this).val() == 'sin-region') {
                swal('Seleccione Región');
            } else if (jQuery(this).val() == 'sin-comuna') {
                swal('Seleccione Comuna');
            }
        });
        jQuery('#regiones').change(function () {
            if (jQuery(this).val() == 'sin-region') {
                swal('Seleccione Región');
            }
        });

        jQuery('#regiones').val(jQuery('#regiones').data('region')).trigger("change");
        jQuery('#comunas').val(jQuery('#comunas').data('comuna')).trigger("change");
    });


    $('#paises').on('change',function(){
        if( $(this).val()==="Chile"){
            $(".region-comuna").show();
        }
        else{
            $(".region-comuna").hide();
        }
    });




    //Slider home
    $('.slider').slick({
        draggable: true,
        arrows: false,
        dots: false,
        fade: true,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 900,
        infinite: true,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        touchThreshold: 100
    });




    // WINDOW SCROLL FUNCTION

    $(window).scroll(function() {

        var scroll = $(window).scrollTop();

        if (scroll >= 250) {
            $("#header").addClass("scrolled");
        } else {
            $("#header").removeClass("scrolled");
        }

    });

    // WINDOW RESIZE FUNCTION

    $(window).resize(function() {



    });

});


/*

$("#nombre-otros").hide();

$('#representacion-evento').on('change',function(){
    if( $(this).val()==="otros"){
        $("#nombre-otros").show();
    }
    else{
        $("#nombre-otros").hide();
    }
});
*/

