<?php /* Template Name: Inscripción COP */ ?>

<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>

<div class="container">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5 mt-5">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="col-lg-12">
            <h2 class="border-top pt-4">Inscribe tu Evento</h2>
            <h4 class="mb-5">Los campos marcados con * son obligatorios</h4>
            <?php echo do_shortcode('[form_inscripcion]');?>
        </div>



    </div>

</div>

<?php get_footer(); ?>
