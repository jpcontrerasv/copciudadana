<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>

<div class="slideshow mb-5">

    <div id="titulo-principal" class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <h1 class="display-3">Inscríbete</h1>
                <h1 class="display-3">Participa</h1>
                <h1 class="display-3">Actúa</h1>
            </div>
        </div>     
    </div>


    <img src="<?php echo get_template_directory_uri(); ?>/img/bg-over.png" alt="" class="bg-over">

    <div class="slider">
        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rand/03.jpg" />
        </div>
        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rand/05.jpg" />
        </div>

        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rand/01.jpg" />
        </div>
        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rand/02.jpg" />
        </div>

        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rand/06.jpg" />
        </div>
    </div>

</div>

<section id="bajada-home" class="container mb-5">
    <div class="row">
        <div class="col-12 text-center mb-5">
            <?php the_field('subtitulo','option'); ?>
        </div>
    </div>
</section>

<section id="bajada" class="container mb-5 pb-5">

    <div class="row">

        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <?php the_field('bajada','option'); ?>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

            <a data-fancybox="" href="https://www.youtube.com/watch?v=TkdPxqpF3tc">
                <img src="<?php echo get_template_directory_uri(); ?>/img/img-video.png" alt="">
            </a>
            <p>Diputado Félix González</p>

        </div>

    </div>

</section>

<section id="cta-home" class="container-fluid mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-11 text-center mb-5">
            <?php the_field('llamado_a_la_accion','option'); ?>
            <div class="clearfix"></div>
            <a href="<?php echo site_url(); ?>/crea-tu-cop-ciudadana/" class="big-cta d-inline-block">
                <i class="fa fa-users" aria-hidden="true"></i> Crea tu COP Ciudadana
            </a>
        </div>
    </div>
</section>

<section id="lista" class="container mb-5">
    <div class="row">
        <div class="col-12 mb-4">
            <h1>Lista de COP Ciudadanas en Chile</h1>
            <p>Acá podrás ver todos los eventos de COP Ciudadana inscritos hasta ahora a través de Chile y el mundo.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <table
                   rel="table-event"
                   data-toggle="table"
                   data-pagination="true"
                   data-page-size="20"
                   data-search="true"
                   data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=wp_event_all_ajax" class="table-striped">

                <thead class="thead-dark">
                    <tr>
                        <th data-field="title" data-sortable="true">Nombre del Organizador</th>
                        <th data-field="country" data-sortable="true">País</th>
                        <th data-field="regions" data-sortable="true">Región</th>
                        <th data-field="commune" data-sortable="true">Comuna</th>
                        <th data-field="link" data-formatter="tableEventList.formatLink">Link</th>
                    </tr>
                </thead>
            </table>
            <script> 
                var tableEventList = new Object();
                tableEventList.formatLink = function(value, row) {
                    return `<a href="${value}">Ver Evento</a>`;
                }
            </script>
        </div>
    </div>

</section>

<section id="blog" class="container mb-5">
    <div class="row">
        <div class="col-lg-12 mb-5">
            <h1 class="mb-2">Blog</h1>
            <!--<p>Blog</p>-->
        </div>
    </div>

    <div class="row">
      <?php query_posts('showposts=3');
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
        
        <div class="card col-lg-4 mb-4" style="width: 18rem;">
            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full'); ?>" class="card-img-top" alt="<?php the_title(); ?>"></a>
            <div class="card-body">
                <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
                <p class="card-text mb-3"><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="btn cta smallest-cta fright">Leer noticia</a>
            </div>
        </div>
        
        <?php endwhile; else:
endif;
wp_reset_query();
?>
        
    </div>
    
    <div class="row">
        <div class="col-lg-12 mb-5 text-right">
            <a href="<?php echo site_url(); ?>/blog">Ver todo el blog</a> 
        </div>
    </div>

</section>

<section id="preguntas-home" class="container">
    <div class="row">
        <div class="col-12 text-center mb-5">
            <h1>Preguntas Frecuentes</h1>
        </div>


        <?php if( have_rows('preguntas_frecuentes', 'option') ):

while ( have_rows('preguntas_frecuentes', 'option') ) : the_row(); ?>
        <div class="col-12 mb-5">
            <h2 class="mb-4"><?php the_sub_field('pregunta'); ?></h2>
            <?php the_sub_field('respuesta'); ?>
        </div>

        <?php endwhile; else : endif; ?>

    </div>
</section>



<?php get_footer(); ?>