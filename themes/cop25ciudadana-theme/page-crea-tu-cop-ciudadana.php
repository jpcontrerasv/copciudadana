<?php /* Template Name: Crea tu COP Ciudadana */ ?>

<?php get_header(); ?>

<?php get_template_part( 'navigation', 'default' ); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
 

<div class="container">

    <div class="row d-flex justify-content-center">

        <div class="col-lg-12 mb-5 mt-5">
            <h1 class="mb-2"><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <div class="col-12">
            <img src="<?php echo get_template_directory_uri(); ?>/img/infografia.jpg">
        </div>

            <a href="<?php echo site_url(); ?>/crea-tu-cop-ciudadana/inscripcion-cop/" class="cta">Comenzar</a>
            

    </div>
    
    
    

</div>


<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>