<?php get_header(); ?>
<?php get_template_part( 'navigation', 'default' ); ?>



<div class="container-fluid">
    <div class="row">
        <div class="col-12 hero-img bg-cover position-relative mb-50" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/rand/03.jpg);">
            &nbsp;
        </div>
    </div>
</div>

<!--CONTENIDO PRINCIPAL-->
<div class="container-fluid mt-5 mb-5">

    <div id="content-single" class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</div>

<div class="container mb-50">
    
    
    
    
    <div class="row">
      <?php query_posts('post_type=post&showposts=1');
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
 
        
        <div class="card col-lg-4 mb-4 border-0">
            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full'); ?>" class="card-img-top" alt="..."></a>
            <div class="card-body">
                <a href="<?php the_permalink(); ?>"><h5 class="card-title"><?php the_title(); ?></h5></a>
                <p class="card-text mb-3"><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="btn cta smallest-cta fright">Leer noticia</a>
            </div>
        </div>
        
        
        <?php endwhile; ?> 
        
        <nav class="w-100 d-flex justify-content-center mb-20" >
            <?php wp_pagenavi(); ?>
        </nav>
        
        
        <?php else:
endif;
wp_reset_query();
?>
       
        
    </div>
    
</div>

<?php get_footer(); ?>