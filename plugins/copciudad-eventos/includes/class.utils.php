<?php 

 class Utils {

     public function get_read_template($template = '', $arg = []) {
        extract($arg, EXTR_SKIP);
        if(file_exists(PATH_EVENTOS_CORP.'/'.$template)){
            ob_start();
            include PATH_EVENTOS_CORP.'/'.$template;
            return ob_get_clean();
        }
        return false;
     }

    
 }