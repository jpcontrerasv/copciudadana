<?php


class EventCop extends Utils {
    public function __construct() {
        add_action('init', array($this, 'create_posttype'), 10);
        add_action('init', array($this, 'handle_action'), 20);

        add_action('template_redirect', array($this, 'handle_event'), 15);

        add_action('show_user_profile', array($this, 'extra_field_user'));
        add_action('edit_user_profile', array($this, 'extra_field_user'));
        add_action('user_new_form', array($this, 'extra_field_user') );
        add_action('personal_options_update', array($this, 'extra_field_user_update'));
        add_action('edit_user_profile_update', array($this, 'extra_field_user_update'));
        add_action('user_register', array($this, 'extra_field_user_update'));
        add_action('login_redirect', array($this, 'redirect_role'), 10, 3);
        add_action('after_setup_theme', array($this, 'remove_admin_bar'));
        
        add_action('wp_enqueue_scripts',  array($this,'script_registrate'));
        add_action('wp_enqueue_scripts',  array($this,'script_inscripcion'));
        add_action('wp_enqueue_scripts',  array($this,'script_profile'));
        add_action('wp_enqueue_scripts',  array($this,'script_event'));
        add_action('wp_enqueue_scripts',  array($this,'script_app'));
        add_action('wp_enqueue_scripts',  array($this,'script_act'));
        add_action('wp_enqueue_scripts',  array($this,'script_message'));

        add_action('wp_head', array($this,'header_regiones'));
        add_action('wp_head', array($this,'handle_url'));

        add_action("wp_ajax_nopriv_ajax_inscription", array($this, 'ajax_inscription'));
        add_action("wp_ajax_ajax_inscription", array ($this, 'ajax_inscription'));
        add_action("wp_ajax_nopriv_ajax_register", array($this, 'ajax_register'));
        add_action("wp_ajax_ajax_register", array ($this, 'ajax_register'));
        add_action("wp_ajax_ajax_profile", array($this, 'ajax_profile'));
        add_action("wp_ajax_nopriv_ajax_profile", array($this, 'ajax_profile'));
        add_action("wp_ajax_ajax_profile_list_participate_event", array($this, 'ajax_profile_list_participate_event'));
        add_action("wp_ajax_nopriv_ajax_profile_list_participate_event", array($this, 'ajax_profile_list_participate_event'));
        add_action("wp_ajax_ajax_profile_my_events", array($this, 'ajax_profile_my_events'));
        add_action("wp_ajax_nopriv_ajax_profile_my_events", array($this, 'ajax_profile_my_events'));
        add_action("wp_ajax_ajax_delete_participant", array($this, 'ajax_delete_participant'));
        add_action("wp_ajax_ajax_update_event", array($this, 'ajax_update_event'));
        add_action("wp_ajax_ajax_update_act", array($this, 'ajax_update_act'));
        add_action("wp_ajax_ajax_delete_event", array($this, 'ajax_delete_event'));
        add_action("wp_ajax_ajax_join_event", array($this, 'ajax_join_event'));
        add_action("wp_ajax_ajax_quit_event", array($this, 'ajax_quit_event'));
        add_action("wp_ajax_ajax_reject_event", array($this, 'ajax_reject_event'));
        add_action("wp_ajax_ajax_approve_event", array($this, "ajax_approve_event"));
        add_action("wp_ajax_ajax_message_event", array($this, "ajax_message_event"));
        add_action("wp_ajax_nopriv_ajax_login", array($this, 'ajax_login'));
        add_action("wp_ajax_wp_event_ajax", array($this, 'wp_event_ajax'));
        add_action("wp_ajax_wp_event_all_ajax", array($this, 'wp_event_all_ajax'));
        add_action("wp_ajax_nopriv_wp_event_all_ajax", array($this, 'wp_event_all_ajax'));

        add_action('admin_init', array($this, 'restrict_admin'), 1 );


        add_shortcode('form_registrate',  array($this, 'shortcode_registrate'));
        add_shortcode('form_inscripcion',  array($this, 'shortcode_inscripcion'));
        add_shortcode('form_perfil', array($this, 'shortcode_perfil'));
        add_shortcode('form_perfil_participante', array($this, 'shortcode_perfil_participante'));
        add_shortcode('form_perfil_eventos', array($this, 'shortcode_perfil_eventos'));
        
        add_filter('wp_mail_content_type', array($this, 'wpmail_set_content_type'));
        add_filter('wp_dropdown_users_args',  array($this, 'display_dropdown_role'), 10, 2);
        
        $this->create_role();
    }

    public function create_posttype() {
        $labels = array(
            'name'                => __('Eventos'),
            'singular_name'       => __('Eventos'),
            'menu_name'           => __('Eventos'),
            'parent_item_colon'   => __('Eventos Padre'),
            'all_items'           => __('Todos los Eventos' ),
            'view_item'           => __('Ver Eventos'),
            'add_new_item'        => __('Agregar Eventos'),
            'add_new'             => __('Nuevo Evento'),
            'edit_item'           => __('Editar Evento'),
            'update_item'         => __('Actualizar Evento'),
            'search_items'        => __('Buscar Evento'),
            'not_found'           => __( 'Not Found'),
            'not_found_in_trash'  => __( 'Not found in Trash'),
        );
        register_post_type( 'Eventos',
            array(
                'labels' => $labels,
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'eventos'),
            )
        );
    
    }

    public function create_role() {
        add_role( 'organizador', 'Organizador', array(
            'read' => true,
            'edit_posts' => true, 
            'publish_posts'=>true, 
            'edit_published_posts'=>true,
            'upload_files'=>true,
            'delete_published_posts'=>true,
        ));

        add_role( 'participante', 'Participante', array(
            'read' => true,
            'edit_posts' => true, 
            'publish_posts'=>true, 
            'edit_published_posts'=>true,
            'upload_files'=>true,
            'delete_published_posts'=>true,
        ));
    }

    public function extra_field_user($user) {
        echo $this->get_read_template('template/admin/user.php', ['user' => $user]);
    }

    public function extra_field_user_update($userId) {
        if (!current_user_can('edit_user', $userId)) {
            return;
        }
        update_user_meta($userId, 'birthday', $_REQUEST['birthday']);
        update_user_meta($userId, 'rut', $_REQUEST['rut']);
        update_user_meta($userId, 'phone', $_REQUEST['phone']);

        update_user_meta($userId, 'country', $_REQUEST['country']);
        update_user_meta($userId, 'regions', $_REQUEST['regions']);
        update_user_meta($userId, 'commune', $_REQUEST['commune']);
    }
  
    public function redirect_role( $redirect_to,  $request, $user) {
        if ( in_array( $user->roles[0], array( 'organizador', 'participante' ) ) )
            return home_url();
        return $redirect_to;
    }
    
    public function remove_admin_bar() {
        if (current_user_can('organizador') || current_user_can('participante')) {
            show_admin_bar(false);
        }
    }


    public function shortcode_registrate() {
        return $this->get_read_template('template/register.php');
    }

    public function shortcode_inscripcion() {
        return $this->get_read_template('template/inscription.php');
    }

    public function shortcode_perfil() {
        $userId = get_current_user_id();
        $user = get_userdata($userId);
        return $this->get_read_template('template/profile.php', ['user' => $user]);
    }
    
    public function shortcode_perfil_participante() {
        return $this->get_read_template('template/profile-participate.php');
    }

    public function shortcode_perfil_eventos() {
        return $this->get_read_template('template/profile-events.php');
    }

    public function script_registrate(){
     wp_enqueue_script( 'registrate-script', URL_EVENTOS_CORP. 'assets/js/register.js',  array ( 'jquery' ), null, false);
    }

    public function script_inscripcion() {
        wp_enqueue_script( 'inscripcion-script', URL_EVENTOS_CORP. 'assets/js/inscription.js',  array ( 'jquery' ), null, false);
    }

    public function script_profile() {
        wp_enqueue_script( 'profile-script', URL_EVENTOS_CORP. 'assets/js/profile.js',  array ( 'jquery' ), null, false);
    }

    public function script_event() {
        if ( 'eventos' === get_post_type() ) {
            wp_enqueue_script( 'events-script', URL_EVENTOS_CORP. 'assets/js/event.js',  array ( 'jquery' ), null, false);
        }
    }

    public function script_app() {
        wp_enqueue_script( 'app-script', URL_EVENTOS_CORP. 'assets/js/app.js',  array ( 'jquery' ), null, true);
    }

    public function script_act() {
        wp_enqueue_script( 'act-script', URL_EVENTOS_CORP. 'assets/js/act.js',  array ( 'jquery' ), null, true);
    }

    public function script_message() {
        if ( 'eventos' === get_post_type() ) {
            wp_enqueue_script( 'message-script', URL_EVENTOS_CORP. 'assets/js/message.js',  array ( 'jquery' ), null, false);
        }
    }

    public function handle_action() {
        if( isset( $_REQUEST['action'] ) ) {
            if($_REQUEST['action'] === REGISTER_KEY_EVENTOS_CORP) {
                $this->register();
            }
            if($_REQUEST['action'] === 'wp_download_event_excel') {
                $this->download_excel_event();
            }
            if($_REQUEST['action'] === 'wp_event_poster') {
                $this->create_event_poster();
            }
        }
    }

    public function wp_event_ajax() {
        $eventos = new WP_Query(array('post_type' => 'eventos', 'posts_per_page' => -1));
        $json = [];
        while($eventos->have_posts()): $eventos->the_post();
            $pais = wp_get_post_terms(get_the_ID(), 'pais');
            $pais = implode(', ',wp_list_pluck($pais,'name'));
            $region = wp_get_post_terms(get_the_ID(), 'regiones');
            $region = implode(', ',wp_list_pluck($region,'name'));
            $estado = get_field('estado');
            $location = '';
            $address = get_field('direccion');
            if(!empty($region) || !empty($pais)) {
                $auxLocation = []; 
                if(!empty($pais)) {
                    $auxLocation[] = $pais;
                }
                if(!empty($region)) {
                    $auxLocation[] = $region;
                }
                $location =  implode(', ', $auxLocation);
                $address .= ", ${location}"; 
            }
            $address .= date_to_es(get_field('fecha_evento'));
            $json[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'detail' => $address,
                'status' => $estado,
                'action' => $estado,
                'link'  => get_permalink(),
            ];
        endwhile;
        die(json_encode($json));
    }

    public function wp_event_all_ajax() {
        $eventos = new WP_Query([
            'post_type' => 'eventos', 'posts_per_page' => -1,
            'meta_query'        => array(
                'relation'      => 'AND',
                array(
                    'key'       => 'estado',
                    'value'     => 'publish',
                    'compare'   => '=',
                ),
            )
        ]);
        $json = [];
        while($eventos->have_posts()): $eventos->the_post();
            $location = '';
            $region = '';
            $pais_term = wp_get_post_terms(get_the_ID(), 'pais');
            $pais = implode(', ',wp_list_pluck($pais_term,'name'));
            $commune_term = wp_get_post_terms(get_the_ID(), 'regiones');
            $commune = implode(', ',wp_list_pluck($commune_term,'name'));
            $estado = get_field('estado');
            $address = get_field('direccion');
            $address .= date_to_es(get_field('fecha_evento'));
            if(isset($commune_term[0]->parent)) {
                $term = get_term( $commune_term[0]->parent, 'regiones' );
                $region = $term->name;
            }
            $json[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'country' => $pais,
                'regions' => $region,
                'commune' => $commune,
                'link' => get_permalink(),
            ];
        endwhile;
        die(json_encode($json));

    }

    public function register_user() {
        $email = sanitize_text_field($_REQUEST['email']);
        $password = sanitize_text_field($_REQUEST['password']);
        $first_name = sanitize_text_field($_REQUEST['first_name']);
        $last_name = sanitize_text_field($_REQUEST['last_name']);
        $country = sanitize_text_field($_REQUEST['country']);
        $regions = sanitize_text_field($_REQUEST['regions']);
        $commune = sanitize_text_field($_REQUEST['commune']);
        if( null == username_exists( $email ) ) {
            $userId = wp_create_user( $email, $password, $email );
            if($userId) {
                wp_update_user(['ID' => $userId, 'first_name' => $first_name, 'last_name' => $last_name, 'role' => 'participante']);
                update_user_meta($userId, 'birthday', $_REQUEST['birthday']);
                update_user_meta($userId, 'rut', $_REQUEST['rut']);
                update_user_meta($userId, 'phone', $_REQUEST['phone']);
                update_user_meta($userId, 'country', $country);
                update_user_meta($userId, 'regions', $regions);
                update_user_meta($userId, 'commune', $commune);
                wp_signon( ['user_login' => $email, 'user_password' => $password, 'remember' => true], false );
                $this->send_mailing_register($userId);
            }
            return $userId;
        }
        return false;
    }

    public function ajax_register() {
        $response = [
            'status' => 'error',
            'message' => 'Error desconocido del sistema.'
        ];
        if(!is_user_logged_in()) {
            $userId = $this->register_user();
            if($userId){
                $myUrl = get_permalink(@get_page_by_path('mi-perfil')->ID);
                $response = [
                    'status' => 'success',
                    'message' => 'El usuario fue creado con exito, te redireccionaremos a tú perfil.',
                    'url' => $myUrl
                ];
                die(json_encode($response));
            }
            $response = [
                'status' => 'error',
                'message' => 'Error al intentar crear el usuario, este ya existe.'
            ];
            die(json_encode($response));
        }
        die(json_encode($response));
    }

    public function ajax_inscription() {
        $userId = get_current_user_id();
        $response = [
            'status' => 'error',
            'message' => 'Error desconocido del sistema.'
        ];
        if(!is_user_logged_in()) {
            $userId = $this->register_user();
            if(!$userId){
                $response = [
                    'status' => 'error',
                    'message' => 'No es posible crear el usuario.'
                ];
                die(json_encode($response));
            }
        }
        if($postId = $this->inscription_user($userId)) {
            $response = [   
                'status' => 'success',
                'message' => 'El evento fue creado de forma correcta.',
                'url' => get_permalink($postId)
            ];
            die(json_encode($response));
        }
        $response = [
            'status' => 'error',
            'message' => 'Error al intentar crear el evento.'
        ];
        die(json_encode($response));
    }

    public function ajax_profile_list_participate_event() {
        $userId = get_current_user_id();
        $args = array(
            'post_type'         => 'eventos',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_query'        => array(
                'relation'      => 'AND',
                array(
                    'key'       => 'estado',
                    'value'     => 'publish',
                    'compare'   => '=',
                ),
                array(
                    'key'       => 'participantes',
                    'value'     => '"'.$userId.'"',
                    'compare'   => 'LIKE',
                )
            )
        );
        $response = [];
        $eventos = new WP_Query($args);
        while($eventos->have_posts()): $eventos->the_post();
            $postId = get_the_ID();
            $direccion = get_field('direccion', $postId);
            $date = get_field('fecha_evento', $postId);
            $url = get_permalink($postId);
            $response[] = [
                'id' => $postId,
                'title' => get_the_title(),
                'address' => $direccion,
                'date' => $date,
                'url' => $url
            ];
        endwhile;
        die(json_encode($response));
    }

    public function ajax_profile_my_events() {
        $userId = get_current_user_id();
        $args = array(
            'post_type'         => 'eventos',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'author__in'        => array($userId),
        );
        $response = [];
        $eventos = new WP_Query($args);
        while($eventos->have_posts()): $eventos->the_post();
            $postId = get_the_ID();
            $direccion = get_field('direccion', $postId);
            $date = get_field('fecha_evento', $postId);
            $url = get_permalink($postId);
            $response[] = [
                'id' => $postId,
                'title' => get_the_title(),
                'address' => $direccion,
                'date' => $date,
                'url' => $url
            ];
        endwhile;
        die(json_encode($response));
    }

    public function ajax_delete_participant() {
        $response = [
            'status' => 'warning',
            'message' => 'No pudo ser eliminado del sistema',
        ];
        $userId = get_current_user_id();
        $eventId = (int) $_REQUEST['event'];
        $participantId = (int) $_REQUEST['user'];
        $post = get_post($eventId);
        if((int) $post->post_author === (int) $userId) {
            $participantes = get_field('participantes', $post->ID);
            $newParticipantes = $this->remove_participant($participantes, 'ID', $participantId);
            update_field('participantes', $newParticipantes, $post->ID);
            $response = [
                'status' => 'success',
                'message' => 'El participante fue eliminado del evento',
            ];
            $this->send_mailing_delete_participant($post, $participantId);
        }
        die(json_encode($response));
    }

    public function ajax_update_act() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo actualizar el acta de conclusiones'
        ];
        $postId = (int) $_REQUEST['post_id'];
        $userId = get_current_user_id();
        $post = get_post($postId);
        $assistants = sanitize_text_field($_REQUEST['assistants']);
        $act_state = sanitize_text_field($_REQUEST['act_state']);
        $act_city = sanitize_text_field($_REQUEST['act_city']);
      
        // $date_format =  DateTime::createFromFormat('d/m/Y h:i A',  "${date} ${hour}");
        // $date = $date_format->format("Y-m-d H:i:s");
        if((int) $post->post_author === (int) $userId) {
            update_field('cantidad_de_asistentes', $assistants, $postId);
            update_field('acta_estado', $act_state, $postId);
            update_field('acta_ciudad', $act_city, $postId);
            $response = [
                'status' => 'success',
                'message' => 'El acta de conclusiones fue actualizada',
            ];
        }
        die(json_encode($response));
    }

    public function ajax_update_event() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo actualizar el evento'
        ];
        $postId = (int) $_REQUEST['post_id'];
        $userId = get_current_user_id();
        $post = get_post($postId);
        $type = sanitize_text_field($_REQUEST['type']);
        $address = sanitize_text_field($_REQUEST['address']);
        $limit = sanitize_text_field($_REQUEST['limit']);
        $date = sanitize_text_field($_REQUEST['date']);
        $hour = sanitize_text_field($_REQUEST['hour']);
        $date_format =  DateTime::createFromFormat('d/m/Y h:i A',  "${date} ${hour}");
        $date = $date_format->format("Y-m-d H:i:s");
        if((int) $post->post_author === (int) $userId) {
            update_field('direccion', $address, $postId);
            update_field('tipo', $type, $postId);
            update_field('limite_de_asistentes', $limit, $postId);
            update_field('fecha_evento', $date, $postId);
            $response = [
                'status' => 'success',
                'message' => 'El evento fue actualizado',
            ];
        }
        die(json_encode($response));
    }

    public function ajax_join_event() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo participar en el evento'
        ];
        $postId = (int) $_REQUEST['event'];
        $post = get_post($postId);
        if(is_user_logged_in() && $post) {
            $userId = get_current_user_id();
            $participantes = get_field('participantes', $postId);
            if($participantes) {
                array_push($participantes, $userId);
                update_field('participantes', $participantes, $postId);
            } else {
                update_field('participantes', $userId, $postId);
            }
            $response = [
                'status' => 'success',
                'message' => 'Estas participando en el evento.',
                'url' => get_permalink($postId),
            ];
        }
        die(json_encode($response));
    }

    public function ajax_quit_event() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo abandonar el evento'
        ];
        $postId = (int) $_REQUEST['event'];
        $post = get_post($postId);
        if(is_user_logged_in() && $post) {
            $userId = get_current_user_id();
            $participantes = get_field('participantes', $post->ID);
            $newParticipantes = $this->remove_participant($participantes, 'ID', $userId);
            update_field('participantes', $newParticipantes, $post->ID);
            $response = [
                'status' => 'success',
                'message' => 'Dejaste de participar en el evento.',
                'url' => get_permalink($postId),
            ];
        }
        die(json_encode($response));
    }

    public function ajax_reject_event() {
        $this->ajax_delete_event();
    }

    public function ajax_approve_event() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo aprobar el evento'
        ];
        $postId = (int) $_REQUEST['event'];
        $post = get_post($postId);
        if(current_user_can('administrator')) {
            update_field('estado', 'publish', $postId);
            $response = [
                'success' => 'success',
                'message' => 'El evento fue aprobado con exito'
            ];
            $this->send_mailing_approve_event($post);
        }
        die(json_encode($response));
    }

    public function ajax_delete_event() {
        $response = [
            'success' => 'warning',
            'message' => 'No se pudo eliminar el evento'
        ];
        $postId = (int) $_REQUEST['event'];
        $post = get_post($postId);
        $userId = get_current_user_id();
        if((int) $post->post_author === (int) $userId || current_user_can('administrator')) {
            $this->send_mailing_delete_event($post);
            wp_delete_post($post->ID);
            $response = [
                'status' => 'success',
                'message' => 'El evento fue eliminado',
                'url' => get_permalink(@get_page_by_path('mi-perfil')->ID),
            ];
        }
        die(json_encode($response));
    }

    public function ajax_message_event() {
        $userId = get_current_user_id();
        $eventId = (int) $_REQUEST['post_id'];
        $message = sanitize_text_field($_REQUEST['message']);
        $post = get_post($eventId);
        $this->send_mailing_message_event($post, $message);
        $response = [
            'status' => 'success',
            'message' => 'El mensaje fue enviado al organizador.',
        ];
        die(json_encode($response));
    }

    public function remove_participant($array, $key, $value) {
        foreach($array as $subKey => $subArray){
                if($subArray[$key] == $value){
                    unset($array[$subKey]);
                }
        }
        return $array;
    
    }

    public function ajax_profile() {
        $response = [
            'status' => 'error',
            'message' => 'Error al intentar actualizar el usuario.'
        ];
        if($this->profile_update()) {
            $response = [
                'status' => 'success',
                'message' => 'El usuario fue actualizado de forma correcta.'
            ];
        }
        die(json_encode($response));
    }

    public function profile_update() {
        $userId = get_current_user_id();
        $password = sanitize_text_field($_REQUEST['password']);
        $first_name = sanitize_text_field($_REQUEST['first_name']);
        $last_name = sanitize_text_field($_REQUEST['last_name']);
        $birthday = sanitize_text_field($_REQUEST['birthday']);
        $rut = sanitize_text_field($_REQUEST['rut']);
        $phone = sanitize_text_field($_REQUEST['phone']);
        $country = sanitize_text_field($_REQUEST['country']);
        $regions = sanitize_text_field($_REQUEST['regions']);
        $commune = sanitize_text_field($_REQUEST['commune']);
        if($userId) {
            wp_update_user(['ID' => $userId, 'first_name' => $first_name, 'last_name' => $last_name]);
            update_user_meta($userId, 'birthday', $birthday);
            update_user_meta($userId, 'rut', $rut);
            update_user_meta($userId, 'phone', $phone);
            update_user_meta($userId, 'country', $country);
            update_user_meta($userId, 'regions', $regions);
            update_user_meta($userId, 'commune', $commune);
            if(!empty($password)) {
                wp_update_user(['ID' => $userId, 'user_pass' => $password]);
            }
            return true;
        }
        return false;
    }
    
    public function inscription_user($userId) {
        $represent = sanitize_text_field($_REQUEST['represent']);
        $title = sanitize_text_field($_REQUEST['title']);
        $date = sanitize_text_field($_REQUEST['date']);
        $hour = sanitize_text_field($_REQUEST['hour']);
        $country = sanitize_text_field($_REQUEST['country']);
        $regions = sanitize_text_field($_REQUEST['regions']);
        $commune = sanitize_text_field($_REQUEST['commune']);
        $address = sanitize_text_field($_REQUEST['address']);
        $type =  sanitize_text_field($_REQUEST['type']);
        $limit =  sanitize_text_field($_REQUEST['limit']);
        $date_format =  DateTime::createFromFormat('d/m/Y h:i A',  "${date} ${hour}");
        $date = $date_format->format("Y-m-d H:i:s");
      

        $myPost = array(
            'post_title'    => wp_strip_all_tags( $title ),
            'post_status'   => 'publish',
            'post_author'   => $userId,
            'post_type' => 'eventos'
        );
        if($postId = wp_insert_post( $myPost )) {
            
            update_field('representacion', $represent, $postId);
            update_field('direccion', $address, $postId);
            update_field('tipo', $type, $postId);
            update_field('limite_de_asistentes', $limit, $postId);
            update_field('fecha_evento', $date, $postId);
            update_field('estado', 'pending', $postId);

            $regions = get_term_by('name', $regions, 'regiones');
            $commune = get_term_by('name', $commune, 'regiones');
            $country = get_term_by('name', $country, 'pais');
            
            if(isset($regions->term_id) && isset($commune->term_id) ) {
                wp_set_post_terms( $postId, $regions->term_id, 'regiones' );
                wp_set_post_terms( $postId, $commune->term_id, 'regiones' );
            }

            if(isset($country->term_id)) {
                wp_set_post_terms( $postId, $country->term_id, 'pais' );
            }
            $this->change_role_organizador($userId);
            $this->send_mailing_inscription($postId);
            return $postId;
        }
        return false;
    }

    public function header_regiones() {
        $taxonomyName = "regiones";
        $parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );   
        $regiones['regiones'] = [];
        $index = 0;
       
        foreach ( $parent_terms as $pterm ) {
            $regiones['regiones'][$index]['NombreRegion'] = $pterm->name;
            $regiones['regiones'][$index]['comunas'] = [];
            $terms = get_terms( $taxonomyName, array( 'parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
            foreach ( $terms as $term ) {
                $regiones['regiones'][$index]['comunas'][] = $term->name;
            }
            $index++;
        }
        $json_region = json_encode($regiones);
        echo "<script type='text/javascript'>var RegionesYcomunas = ${json_region};</script>";
    }

    public function handle_url() {
        $url = get_bloginfo('url');
        echo "<script type='text/javascript'>var baseURL = '${url}';</script>";
    }

    public function change_role_organizador($userId) {
        $u = get_userdata( $userId );
        $u->remove_role( 'participante' );
        $u->add_role( 'organizador' );
    }

    public function wpmail_set_content_type(){
        return "text/html";
    }

    public function send_mailing_register($userId) {
        $user = get_userdata($userId);
        $html = $this->get_read_template('template/email/register.php', ['user' => $user]);
        wp_mail( $user->user_email, 'Bienvenido a la #COP25Ciudadana', $html );
    }

    public function send_mailing_inscription($postId) {
        $post = get_post($postId);
        if($post) {
            $user = get_userdata($post->post_author);
            $html = $this->get_read_template('template/email/inscription.php', ['post' => $post, 'user' => $user]);
            wp_mail( $user->user_email, ' #COP25Ciudadana - Inscripción de Evento', $html );
        }
    }

    public function send_mailing_delete_event($post) {
        $participantes = get_field('participantes', $post->ID);
        foreach($participantes as $participante) {
            $user = get_userdata($participante['ID']);
            $html = $this->get_read_template('template/email/event-delete.php', ['user' => $user, 'post' => $post]);
            wp_mail( $user->user_email, '#COP25Ciudadana - Evento eliminado', $html );
        }
    }

    public function send_mailing_approve_event($post) {
        $user = get_userdata($post->post_author);
        $html = $this->get_read_template('template/email/event-approve.php', ['user' => $user, 'post' => $post]);
        wp_mail( $user->user_email, '#COP25Ciudadana - Tu evento ha sido aprobado', $html ); 
    }

    public function send_mailing_message_event($post, $message) {
        $user = get_userdata($post->post_author);
        $html = $this->get_read_template('template/email/event-message.php', ['user' => $user, 'message' => $message]);
        wp_mail( $user->user_email, '#COP25Ciudadana - Tienes un nuevo mensaje', $html ); 
    }

    public function send_mailing_reject_event($post) {
        $user = get_userdata($post->post_author);
        $html = $this->get_read_template('template/email/event-reject.php', ['user' => $user, 'post' => $post]);
        wp_mail( $user->user_email, '#COP25Ciudadana - Tu evento ha sido rechazado', $html );
    }

    public function send_mailing_delete_participant($post, $userId) {
        $user = get_userdata($userId);
        $html = $this->get_read_template('template/email/event-delete-participant.php', ['user' => $user, 'post' => $post]);
        wp_mail( $user->user_email, '#COP25Ciudadana - Has sido removido de un evento', $html );
    }

    
   

    public function restrict_admin() {
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
        return;
        if ((current_user_can('organizador') || current_user_can('participante')) 
        && is_admin() && !current_user_can('administrator')) {
          wp_redirect(get_bloginfo('url'));
          exit;
        }
    }
 
    public function handle_event() {
        global $post;
        if($post->post_type == 'eventos') {
            $estado = get_field('estado', $post->ID);
            $userId = get_current_user_id();
           
            if($estado !== 'publish') {
             
                if(((int) $userId !== (int) $post->post_author) ){
                    if( !current_user_can('administrator')) {
                        wp_redirect(get_bloginfo('url'));
                        exit;
                    }
                }
            } 
        }
       
    }

   
    public function display_dropdown_role($query_args, $r) {
        if (isset($r['name']) && $r['name'] === 'post_author_override') {
            if (isset($query_args['who'])) {
                unset($query_args['who']);
            }
            $query_args['role__in'] = array('administrator', 'organizador');
        }
        return $query_args;
    }

    
    function ajax_login() {
        $response = [
            'status' => 'warning',
            'message' => 'El usuario o contraseña es incorrecto',
        ];
        $email = sanitize_text_field($_REQUEST['email']);
        $password = sanitize_text_field($_REQUEST['password']);
        $login = wp_signon( ['user_login' => $email, 'user_password' => $password, 'remember' => true], false );
        if (!is_wp_error($login) ){
            $response = [
                'status' => 'success',
                'url' => home_url('/mi-perfil'),
            ];
        }
        die(json_encode($response));
    }

    public function download_excel_event() {
        $exporter = new ExportDataExcel('browser', 'eventos-'.date('d-m-Y').'.xls');
        $exporter->initialize();
        $exporter->addRow([
            "Id", "Nombre", "Representación",
            "Dirección",  "País", "Región",
            "Ciudad", "Fecha Evento", "Limite",
            "Estado", "Organizador Nombre",
            "Organizador Email", "Organizador Teléfono"
        ]); 
        $eventos = new WP_Query(array('post_type' => 'eventos', 'posts_per_page' => -1));
        while($eventos->have_posts()): $eventos->the_post();
            $user = get_userdata(get_the_author_meta('ID'));
            $estado = get_field('estado');
            $id = get_the_ID();
            $titulo = get_the_title();
            $representacion = get_field('representacion');
            $direccion = get_field('direccion');
            $fecha = date_to_es(@get_field('fecha_evento'));
            $limite = get_field('limite_de_asistentes');
            $pais = wp_get_post_terms(get_the_ID(), 'pais');
            $pais = implode(', ',wp_list_pluck($pais,'name'));
            $commune_term = wp_get_post_terms(get_the_ID(), 'regiones');
            $commune = implode(', ',wp_list_pluck($commune_term,'name'));
            $name_user = $user->first_name.' '.$user->last_name;
            $email_user = $user->user_email;
            $phone_user = get_user_meta($user->ID, 'phone', true);
            if(isset($commune_term[0]->parent)) {
                $term = get_term( $commune_term[0]->parent, 'regiones' );
                $region = $term->name;
            }
            if($estado === 'publish') {
                $estado = 'Aprobado';
            } else if($estado === 'pending') {
                $estado = 'Pendiente';
            } else {
                $estado = 'Rechazado';
            }
            $exporter->addRow([
                $id, $titulo, $representacion,
                $direccion, @$pais, @$region,
                @$commune, $fecha, $limite,
                @$estado, $name_user, $email_user,
                $phone_user
            ]);
        endwhile;
        $exporter->finalize(); 
        exit();
    }

    public function create_event_poster() {
        $postId = $_REQUEST['id'];
        $post = get_post($postId);
        $userId = get_current_user_id();
        if((int) $post->post_author === (int) $userId || current_user_can('administrator')) {
            $direccion = get_field('direccion', $post->ID);
            $pais_term = wp_get_post_terms($post->ID, 'pais');
            $pais = implode(', ',wp_list_pluck($pais_term,'name'));
            $commune_term = wp_get_post_terms($post->ID, 'regiones');
            $commune = implode(', ',wp_list_pluck($commune_term,'name'));
            $estado = get_field('estado');
            $address = get_field('direccion');
            $address .= date_to_es(get_field('fecha_evento'));
            if(isset($commune_term[0]->parent)) {
                $term = get_term( $commune_term[0]->parent, 'regiones' );
                $region = $term->name;
            }
            $text = $direccion."\n";
            if(!empty($commune)) {
                if(!empty($commune)) {
                    $text .= $commune. ", \n";
                }
                if(!empty($region)) {
                    $text .= $region. "\n";
                }
            } else {
                $text .= $pais. "\n";
            }

            $fecha = get_date_event(get_field('fecha_evento', $post->ID));
            $hora =  get_hour_event(get_field('fecha_evento', $post->ID));

            $img = imagecreatefromjpeg(PATH_EVENTOS_CORP.'/template/afiche/afiche.jpg');
            
            $title = new Box($img);
            $title->setFontSize(150);
            $title->setFontFace(PATH_EVENTOS_CORP.'/template/afiche/fonts/ABCNormal-Bold.ttf');
            $title->setFontColor(new Color(105, 187, 111));
            $title->setBox(
                220,  // distance from left edge
                750,  // distance from top edge
                2100, // textbox width
                800  // textbox height
            );
            $title->setTextAlign('center', 'center');        
            $title->draw(mb_strtoupper($post->post_title));

            $direccionbox = new Box($img);
            $direccionbox->setFontSize(70);
            $direccionbox->setFontFace(PATH_EVENTOS_CORP.'/template/afiche/fonts/ABCNormal-Bold.ttf');
            $direccionbox->setFontColor(new Color(000, 000, 000));
            $direccionbox->setBox(
                195,  // distance from left edge
                1820,  // distance from top edge
                1000, // textbox width
                1000  // textbox height
            );
            $direccionbox->setTextAlign('center', 'top');
            $direccionbox->draw($text);
            

            $fechabox = new Box($img);
            $fechabox->setFontSize(75);
            $fechabox->setFontFace(PATH_EVENTOS_CORP.'/template/afiche/fonts/ABCNormal-Bold.ttf');
            $fechabox->setFontColor(new Color(000, 000, 000));
            $fechabox->setBox(
                1300,  // distance from left edge
                1820,  // distance from top edge
                800, // textbox width
                400  // textbox height
            );
            $fechabox->setTextAlign('left', 'top');
            $fechabox->draw($fecha);


            $hourbox = new Box($img);
            $hourbox->setFontSize(75);
            $hourbox->setFontFace(PATH_EVENTOS_CORP.'/template/afiche/fonts/ABCNormal-Bold.ttf');
            $hourbox->setFontColor(new Color(000, 000, 000));
            $hourbox->setBox(
                1300,  // distance from left edge
                2180,  // distance from top edge
                800, // textbox width
                400  // textbox height
            );
            $hourbox->setTextAlign('left', 'top');
            $hourbox->draw($hora);

            header("Content-type: image/png");
            imagepng($img);
        }
    }
}

$events = new EventCop();