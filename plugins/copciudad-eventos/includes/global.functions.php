<?php

if(!function_exists("redirect_private_page")) {
    function redirect_private_page($allow = false, $redirect = false) {
        $user = wp_get_current_user();
        if ($allow) {
            $intersect = (array_intersect($allow, (array) $user->roles));
            if(count($intersect) === 0) {
                if($redirect) {
                    wp_redirect($redirect);
                    exit;
                }
                wp_redirect(get_bloginfo('url'));
                exit;
            }
        }
    }
}


if(!function_exists("is_private_event")) {
    function is_private_event() {
        global $post;
        if(isset($post->ID)) {
            $type = get_field('tipo', $post->ID);
            if($type === 'private') {
                return true;
            }
        }
        return false;
    }
}

if(!function_exists("date_to_es"))  {
    function date_to_es($date) {
        $date = explode('/', $date);
        $month = ['Enero', 'Febrero', 'Marzo', 'Abri', 'Mayo', 'Junio', 'Julio',
         'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        if(is_array($date)) {
            return @$date[0].' '.@$month[(int) $date[1]- 1]. ' '. @$date[2];
        }
    }
}


if(!function_exists("get_hour_event"))  {
    function get_hour_event($date) {
        $date = explode(' ', $date);
        if(is_array($date)) {
            return @$date[1].' '.strtoupper(@$date[2]);
        }
    }
}

if(!function_exists("get_date_event"))  {
    function get_date_event($date) {
        $date = explode('/', $date);
        $month = ['Enero', 'Febrero', 'Marzo', 'Abri', 'Mayo', 'Junio', 'Julio',
         'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        if(is_array($date)) {
            $year = explode(' ',@$date[2]);
            return @$date[0].' '.@$month[(int) $date[1]- 1]. ' '.@$year[0];
        }
    }
}



if(!function_exists("is_participate")) {
    function is_participate() {
        global $post;
        $user = wp_get_current_user();
        $is = false;
        if(isset($post->ID)) {
            $participantes = get_field('participantes', $post->ID);
            if(count($participantes) !== 0) {
                foreach($participantes as $participante){
                    if($participante["ID"] === $user->ID){
                        $is = true;
                    }
                }
            }
            return $is;
        }
    }
}

if(!function_exists("count_participate_event")) {
    function count_participate_event() {
        global $post;
        if(isset($post->ID)) {
            $participantes = get_field('participantes', $post->ID);
            return count($participantes);
        }
        return false;
    }
}

if(!function_exists("count_available_event")) {
    function count_available_event() {
        global $post;
        if(isset($post->ID)) {
            $participantes = get_field('participantes', $post->ID);
            $limite = get_field('limite_de_asistentes', $post->ID);
            if(is_array($participantes)){
                $available = $limite - count($participantes);
                return $available;
            }
            return $limite;
        }
        return false;
    }
}


if(!function_exists("owner_event")) {
    function owner_event() {
        global $post;
        $user = wp_get_current_user();
        if(isset($post->ID)) {
            if((int) $user->ID === (int) $post->post_author) {
                return true;
            }
        }
        return false;
    }
}

if(!function_exists("is_open_event")) {
    function is_open_event(){
        global $post;
        $fecha_evento = get_field('fecha_evento', $post->ID);
        $date_format =  DateTime::createFromFormat('d/m/Y h:i A',  "$fecha_evento", new DateTimeZone('America/Santiago'));
        $timezone = new DateTimeZone('America/Santiago');
        $now = new DateTime();
        $now->setTimezone($timezone);
        if($date_format > $now) {
            return true;
        } 
        return false;
    }
}