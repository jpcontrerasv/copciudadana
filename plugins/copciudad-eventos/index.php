<?php
/*
Plugin Name: Eventos CopCiudad
Description: Plugin con shortcode para el registro de Eventos y manejo de roles
Author: Guillermo Fuentes
Version: 1.6.2
License: GPL-2.0+
*/

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
define('REGISTER_KEY_EVENTOS_CORP', '2JBUABIWCY');
define('INSCRIPTION_KEY_EVENTOS_CORP', '4JEUXBIWCY');

define('PATH_EVENTOS_CORP', dirname(__FILE__));
define('URL_EVENTOS_CORP', plugin_dir_url( __FILE__ ));

require(PATH_EVENTOS_CORP.'/includes/gd/Box.php');
require(PATH_EVENTOS_CORP.'/includes/gd/Color.php');
include(PATH_EVENTOS_CORP.'/includes/global.functions.php');
include(PATH_EVENTOS_CORP.'/includes/class.excel.php');
include(PATH_EVENTOS_CORP.'/includes/class.utils.php');
include(PATH_EVENTOS_CORP.'/includes/class.events.php');
