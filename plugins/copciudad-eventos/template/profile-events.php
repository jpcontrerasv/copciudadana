<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre del Evento</th>
            <th scope="col">Link</th>
        </tr>
    </thead>
    <tbody id="render-profile-events">
        
    </tbody>
</table>

<script id="template-profile-events" type="text/template">
    <tr>
        <th scope="row">{{id}}</th>
        <td>{{title}}</td>
        <td><a href="{{url}}" target="_blank">Ir al Evento</a></td>
    </tr>
</script>
