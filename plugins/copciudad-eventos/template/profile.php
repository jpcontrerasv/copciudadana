<form id="profile" rel="profile" method="post">
    <div class="alert" style="display:none;" rel="notice"></div>
    <div class="form-group container-fluid">
        <div class="row">
            <div class="col-6 pl0">
                <label class="d-block">Nombres <span>*</span></label>
                <small>Ingresa tu(s) nombre(s).</small>

                <input type="text" value="<?php echo $user->first_name;?>" name="first_name" required>

            </div>
            <div class="col-6">
                <label class="d-block">Apellidos <span>*</span></label>
                <small>Ingresa tus apellidos.</small>
                <input type="text"  value="<?php echo $user->last_name;?>" name="last_name"  required>

            </div>
        </div>


    </div>

    <div class="form-group container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 pl0">
                <label>R.U.T.</label>
                <?php $rut = get_user_meta($user->ID, 'rut', true);?>
                <input type="text" id="rut" value="<?php echo $rut;?>" name="rut" >
                <small>Ingresa tu RUT <strong>sin puntos ni guiones</strong>. En el caso de no contar con un RUT chileno puedes dejar este campo en blanco.</small>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8 col-sm-9 pl0">
                <label>Fecha de Nacimiento <span>*</span></label>
                <?php $birthday = get_user_meta($user->ID, 'birthday', true);?>
                <input type="text" id="fecha-de-nacimiento" readonly required  name="birthday"  value="<?php echo $birthday;?>">
                <small>Ingresa tu Fecha de Nacimiento.</small>
            </div>
        </div>


    </div>


    <div class="form-group">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 pl0">
                    <label>País <span>*</span></label>

                    <select id="paises" class="select-css" name="country">


                        <option disabled>Selecciona País</option>
                        <?php $country = get_user_meta($user->ID, 'country', true);?>
                        <option value="Chile" id="chile" <?php if($country === 'Chile') {?>  selected <?php } ?>>Chile</option>
                        <option value="--" disabled>-----------</option>
                    
                        <!--<option value="Elegir" id="AF">Elegir opción</option>-->
                        <?php
                            $terms = get_terms( array( 
                                'taxonomy' => 'pais',
                                'parent'   => 0,
                                'hide_empty' => false
                            ) );
                            foreach($terms as $term) {
                                $pref = substr($term->name, 0,2);
                        ?>
                        <option value="<?php echo $term->name;?>" <?php if($country === $term->name) {?> selected <?php } ?>><?php echo $term->name;?></option>
                        <?php 
                            }
                        ?>
                    </select>

                </div>
                <div class="col-4 region-comuna">
                    <?php $regions = get_user_meta($user->ID, 'regions', true);?>
                    <label>Región <span>*</span></label>
                    <select id="regiones" class="select-css" data-region="<?php echo $regions;?>" name="regions"></select>
                </div>
                <div class="col-4 region-comuna">
                    <?php $commune = get_user_meta($user->ID, 'commune', true);?>
                    <label>Comuna <span>*</span></label>
                    <select id="comunas" class="select-css" data-comuna="<?php echo $commune;?>" name="commune"></select>
                </div>
            </div>     
        </div>
    </div>


    <div class="form-group container-fluid">
        <div class="row">
            <div class="col-6 pl0 mb-4">
                <label>Email <span>*</span></label>

                <input type="email" value="<?php echo $user->user_email;?>" name="email" readonly required>
                <small>nombre@email.com</small>
            </div>
            <div class="col-6 mb-4">
                <label>Teléfono <span>*</span></label>
                <?php $phone = get_user_meta($user->ID, 'phone', true);?>
                <input type="tel" value="<?php echo $phone;?>"  name="phone" required>
                <small>Móvil o Fijo.</small>
            </div>

            <div class="col-6 pl0 mb-4">
                <label>Contraseña </label>

                <input type="password"  name="password">
                <small>Te recomendamos usar símbolos, mayúsculas y números.</small>
            </div>
            <div class="col-6 mb-4">
                <label>Repite tu Contraseña </label>

                <input type="password" name="repassword" equals="password" >
                <small>Móvil o Fijo.</small>
            </div>

        </div>


    </div>



    <input type="submit" value="Actualizar mi perfil">

</form>