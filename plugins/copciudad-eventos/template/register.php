<form id="registro-evento" rel="register" method="post">
<div class="alert" style="display:none;" rel="notice"></div>
<div class="form-group container-fluid">
    <div class="row">
        <div class="col-6 pl0">
            <label>Nombres <span>*</span></label>

            <input type="text" required="required"  name="first_name">
            <small>Ingresa tu(s) nombre(s).</small>
        </div>
        <div class="col-6">
            <label>Apellidos <span>*</span></label>

            <input type="text" required  name="last_name">
            <small>Ingresa tus apellidos.</small>
        </div>
    </div>


</div>

<div id="div-rut-chileno"  class="form-group container-fluid">
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 pl0">
            <label>R.U.T.</label>

            <input type="text" id="rut"  name="rut">
            <small>Ingresa tu RUT <strong>sin puntos ni guiones</strong>. En el caso de no contar con un RUT chileno puedes dejar este campo en blanco.</small>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8 col-sm-9 pl0">
            <label>Fecha de Nacimiento <span>*</span></label>

            <input type="text" id="fecha-de-nacimiento"  name="birthday" required>
            <small>Ingresa tu Fecha de Nacimiento.</small>
        </div>
    </div>


</div>
<div class="form-group">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 pl0">
                    <label>País <span>*</span></label>

                    <select id="paises" class="select-css" name="country">


                        <option disabled>Selecciona País</option>
                        <?php $country = get_user_meta($user->ID, 'country', true);?>
                        <option value="Chile" id="chile" <?php if($country === 'Chile') {?>  selected <?php } ?>>Chile</option>
                        <option value="--" disabled>-----------</option>
                    
                        <!--<option value="Elegir" id="AF">Elegir opción</option>-->
                        <?php
                            $terms = get_terms( array( 
                                'taxonomy' => 'pais',
                                'parent'   => 0,
                                'hide_empty' => false
                            ) );
                            foreach($terms as $term) {
                                $pref = substr($term->name, 0,2);
                        ?>
                        <option value="<?php echo $term->name;?>" <?php if($country === $term->name) {?> selected <?php } ?>><?php echo $term->name;?></option>
                        <?php 
                            }
                        ?>
                    </select>

                </div>
                <div class="col-4 region-comuna">
                    <?php $regions = get_user_meta($user->ID, 'regions', true);?>
                    <label>Región <span>*</span></label>
                    <select id="regiones" class="select-css" data-region="<?php echo $regions;?>" name="regions"></select>
                </div>
                <div class="col-4 region-comuna">
                    <?php $commune = get_user_meta($user->ID, 'commune', true);?>
                    <label>Comuna <span>*</span></label>
                    <select id="comunas" class="select-css" data-comuna="<?php echo $commune;?>" name="commune"></select>
                </div>
            </div>     
        </div>
    </div>

<div class="form-group container-fluid">
    <div class="row">
        <div class="col-6 pl0">
            <label>Email <span>*</span></label>

            <input type="email" name="email" required>
            <small>nombre@email.com</small>
        </div>
        <div class="col-6">
            <label>Teléfono <span>*</span></label>

            <input type="tel"  name="phone" required>
            <small>Móvil o Fijo.</small>
        </div>
        
        <div class="col-6 pl0">
            <label>Contraseña <span>*</span></label>

            <input type="password" required name="password">
            <small>Te recomendamos usar símbolos, mayúsculas y números.</small>
        </div>
        <div class="col-6">
            <label>Repite tu Contraseña <span>*</span></label>

            <input type="password" required name="repassword" equals="password">
            <small>Móvil o Fijo.</small>
        </div>
        
    </div>


</div>

<div class="form-check pb-5">
    <input class="form-check-input" type="checkbox" value="" id="aceptartyc" checked required>
    <label class="form-check-label" for="aceptartyc">
        &nbsp;&nbsp<small>Al presionar Enviar acepto los <a href="<?php echo site_url(); ?>/terminos-y-condiciones" target="_blank">Términos y Condiciones</a></small> 
    </label>
</div>
<input type="hidden" value="<?php echo REGISTER_KEY_EVENTOS_CORP;?>" name="action">
<input type="submit" value="Enviar" id="yourbutton">

<div class="d-block" id="summary"></div>


</form>