<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre del Evento</th>
            <th scope="col">Dirección</th>
            <th scope="col">Fecha</th>
            <th scope="col">Link</th>
        </tr>
    </thead>
    <tbody id="render-profile-participate">
       
    </tbody>
</table>
<script id="template-profile-participate" type="text/template">
    <tr>
        <th scope="row">{{id}}</th>
        <td>{{title}}</td>
        <td>{{address}}</td>
        <td>{{date}}</td>
        <td><a href="{{url}}" target="_blank">Ir al Evento</a></td>
    </tr>
</script>