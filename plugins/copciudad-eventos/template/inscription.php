
<form id="registro-evento" rel="inscription">
    <div class="alert" style="display:none;" rel="notice"></div>
    <div class="form-group">
        <label>¿Este evento se organiza en representación de? <span>*</span></label>

        <select id="representacion-evento" class="select-css" required name="represent">
            <option selected disabled value="">Selecciona una opción</option>
            <option>Junta de vecinos</option>
            <option>Colegio</option>
            <option>Curso</option>
            <option>Municipios</option>
            <option>Universidad</option>
            <option>Centro de Alumnos</option>
            <option>Familiar</option>
            <option>Organización Ambiental</option>
            <option value="otros" id="otros">Otros</option>
        </select>
    </div>

    <div id="nombre-otros" class="form-group">
        <label>¿Cuál es el nombre de tu Institución, Organización o Grupo? <span>*</span></label>

        <input type="text" required name="title">

        <small>Ejemplo: "Sindicato Nº1 de Estibadores", "Familia Contreras Valdés", "Carrera de Obstetricia".</small>

    </div>

    <div class="form-group">
        <label>Fecha y Hora del Evento</label>
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 pl0">
                    <p>Fecha <span>*</span></p>
                    <input type="text" id="pickdate" name="date" required>
                </div>
                <div class="col-6">
                    <p>Hora <span>*</span></p>
                    <input type="text" id="picktime" name="hour" required>
                </div>
            </div>     
        </div>


    </div>

    <div class="form-group">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 pl0">
                    <label>País <span>*</span></label>

                    <select id="paises" class="select-css" name="country">


                        <option disabled value="">Selecciona País</option>

                        <option value="Chile" id="chile" selected>Chile</option>
                        <option value="--" disabled>-----------</option>

                        <!--<option value="" id="AF">Elegir opción</option>-->
                        <?php
                            $terms = get_terms( array( 
                                'taxonomy' => 'pais',
                                'parent'   => 0,
                                'hide_empty' => false
                            ) );
                            foreach($terms as $term) {
                                $pref = substr($term->name, 0,2);
                        ?>
                        <option value="<?php echo $term->name;?>"><?php echo $term->name;?></option>
                        <?php 
                            }
                        ?>


                    </select>

                </div>
                <div class="col-4 region-comuna">
                    <label>Región <span>*</span></label>
                    <select id="regiones" name="regions" class="select-css"></select>
                </div>
                <div class="col-4 region-comuna">
                    <label>Comuna <span>*</span></label>
                    <select id="comunas" name="commune" class="select-css"></select>
                </div>
            </div>     
        </div>
    </div>

    <div class="form-group">
        <label>Dirección</label>

        <input type="text" required name="address">
        <small>Ingresa la dirección donde ocurrirá el evento.</small>

    </div>

    <div class="form-group">
        <label>¿Evento Privado o Público?</label>

        <div class="container-fluid">
            <div class="row">

                <div class="col-6 pl0">
                    <p class="form__answer"> 
                        <input type="radio" name="type" id="match_2" value="public" checked> 
                        <label for="match_2" class="checkbox">
                            Público
                            <br>
                            <i class="fa fa-unlock-alt fa-3x" aria-hidden="true"></i>
                        </label> 

                    </p>
                    <p>Este evento estará disponible para que cualquiera pueda asistir, su ubicación, fecha y hora de inicio serán informados a todos quienes quieran participar.
                        <br>
                        <small>Ejemplo: Evento COP Ciudadana en Junta de Vecinos.</small></p>
                </div>



                <div class="col-6">
                    <p class="form__answer"> 
                        <input type="radio" name="type" id="match_1" value="private"> 
                        <label for="match_1" class="checkbox">
                            Privado
                            <br>
                            <i class="fa fa-lock fa-3x" aria-hidden="true"></i>
                        </label> 
                    </p>
                    <p>Este evento estará publicado en la lista de eventos, pero no podrá verse ni el lugar ni la hora de inicio.<br>
                        <small>Ejemplo: Evento COP Familiar.</small></p>
                </div>
            </div>
        </div>

    </div>

    <div class="form-group col-6 pl0">
        <label>Límite de asistentes <span>*</span></label>
        <p>¿Cuántas personas pueden inscribirse en tu evento?</p>


        <input type="number" required name="limit">
        <small>Ingresa sólo números. </small>

    </div>

    <?php if(!is_user_logged_in()) { ?>
    <h2 class="mb-5 border-top pt-4">Tu Cuenta</h2>
    <p>Para poder registrar este evento debes crear una cuenta de usuario.</p>
    <div class="form-group container-fluid">
        <div class="row">
            <div class="col-6 pl0">
                <label>Nombres <span>*</span></label>

                <input type="text" name="first_name" required>
                <small>Ingresa tu(s) nombre(s).</small>
            </div>
            <div class="col-6">
                <label>Apellidos <span>*</span></label>

                <input type="text" name="last_name" required>
                <small>Ingresa tus apellidos.</small>
            </div>
        </div>


    </div>

    <div id="div-rut-chileno"  class="form-group container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 pl0">
                <label>R.U.T.</label>

                <input type="text" id="rut" name="rut">
                <small>Ingresa tu RUT <strong>sin puntos ni guiones</strong>. En el caso de no contar con un RUT chileno puedes dejar este campo en blanco.</small>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8 col-sm-9 pl0">
                <label>Fecha de Nacimiento <span>*</span></label>

                <input type="text" id="fecha-de-nacimiento" name="birthday" required>
                <small>Ingresa tu Fecha de Nacimiento.</small>
            </div>
        </div>


    </div>


    <div class="form-group container-fluid">
        <div class="row">
            <div class="col-6 pl0">
                <label>Email <span>*</span></label>

                <input type="email" name="email" required>
                <small>nombre@email.com</small>
            </div>
            <div class="col-6">
                <label>Teléfono <span>*</span></label>

                <input type="tel" name="phone" required>
                <small>Móvil o Fijo.</small>
            </div>
            
            <div class="col-6 pl0">
                <label>Contraseña <span>*</span></label>

                <input type="password" name="password" required>
                <small>Te recomendamos usar símbolos, mayúsculas y números.</small>
            </div>
            <div class="col-6">
                <label>Repite Contraseña <span>*</span></label>

                <input type="password"  name="repassword" equals="password" required>
                <small>Te recomendamos usar símbolos, mayúsculas y números.</small>
            </div>
            
        </div>


    </div>

    <div class="form-check pb-5">
        <input class="form-check-input" type="checkbox" value="" id="aceptartyc" checked required>
        <label class="form-check-label" for="aceptartyc">
            &nbsp;&nbsp<small>Al presionar Enviar acepto los <a href="<?php echo site_url(); ?>/terminos-y-condiciones" target="_blank">Términos y Condiciones</a></small> 
        </label>
    </div>
    <?php } ?>
    <div class="form-group text-center">
    <input type="submit" class="mx-auto" value="Enviar" id="yourbutton">
    </div>
    <div class="d-block" id="summary"></div>
</form>