<h3><?php esc_html_e( 'Información Adicional' ); ?></h3>
<table class="form-table">
    <tr class="form-field">
        <th><label for="rut"><?php esc_html_e( 'R.U.T' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'rut', true)); ?>" name="rut" id="rut" ></td>
    </tr>
    <tr class="form-field">
        <th><label for="birthday"><?php esc_html_e( 'Fecha de Nacimiento' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'birthday', true)); ?>" name="birthday" id="birthday" ></td>
    </tr>
    <tr class="form-field">
        <th><label for="phone"><?php esc_html_e( 'Teléfono' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'phone', true)); ?>" name="phone" id="phone" ></td>
    </tr>
    


    <tr class="form-field">
        <th><label for="country"><?php esc_html_e( 'País' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'country', true)); ?>" name="country" id="country" ></td>
    </tr>
    <tr class="form-field">
        <th><label for="regions"><?php esc_html_e( 'Región' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'regions', true)); ?>" name="regions" id="regions" ></td>
    </tr>
    <tr class="form-field">
        <th><label for="commune"><?php esc_html_e( 'Comuna' ); ?></label></th>
        <td><input type="text" value="<?php echo esc_attr(get_user_meta($user->ID, 'commune', true)); ?>" name="commune" id="commune" ></td>
    </tr>
</table>