<!DOCTYPE html>
<html>
    <body>
        <table width="600" style="margin:0 auto 0 auto; border-collapse:collapse; border:1px solid #000; background:#FFF; font-family:Arial, Helvetica, sans-serif;" align="center" border="0" cellspacing="0" cellpadding="0">
            <!-- HEADER -->
            <tbody><tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="600" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <img src="https://www.ecologistas.cl/wp-content/uploads/2016/10/LOGO-PEV-WEB-2.png" alt="Partido Ecologista Verde" width="200" height="79" style="display:block; margin:0 auto; border:0px" border="0">
                            </td>
                        </tr>

                    </tbody></table>

                </td>

            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:28px; line-height:18px; font-weight:bold; text-align:center;">
                    #COP25Ciudadana
                </td>
            </tr>

            <tr>
                <td width="600" height="190">
                    <img src="http://cop25ciudadana.cl/mail-img/head-mail-PEV.jpg" alt="Partido Ecologista Verde" width="600" height="190" style="display:block; margin:0 auto; border:0px" border="0">
                </td>
            </tr>

            <tr>
                <td width="600" height="60" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; text-align:center;">
                    <h1>Has creado un evento</h1>
                </td>
            </tr>
                
            <tr>
                <td width="600" height="40" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:16px; text-align:center;">
                    ¡Gracias <?php echo $user->first_name;?> por crear un evento!
                </td>
            </tr>


            <!--BODY-->
            <tr>

                <td width="600" height="50" style="padding-top:10px; padding-bottom:30px;">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="100" height="50">
                                &nbsp;
                            </td>

                            <td width="400" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#474B50; vertical-align:middle; mso-line-height-rule:exactly; font-size:16px; line-height:20px; text-align:center;">
                                <p>Tu evento <strong><?php echo $post->post_title;?></strong> ha sido creado y está <strong>pendiente de aprobación</strong>.</p>
                                <p>Recibirás un correo cuando tu evento sea aprobado.</p>
                                <p>Fecha: <?php the_field('fecha_evento', $post->ID);?></p>
                                <p>Lugar: <?php the_field('direccion', $post->ID);?></p>
                            </td>


                            <td width="100" height="50">
                                &nbsp;
                            </td>


                        </tr>

                    </tbody></table>

                </td>

            </tr>

            

            <!--FOOTER-->
            <tr>
                <td width="600" height="50" style="text-align:center;">
                    Síguenos en nuestras redes

                </td>
            </tr>
            <tr>

                <td width="600" height="50">

                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://es-la.facebook.com/PartidoEcologistaVerde/" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-facebook.png" alt="Facebook Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://twitter.com/ecologistachile" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-twitter.png" alt="Twitter Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>
                            <td width="200" height="50" style="text-align:center; padding-top:10px; padding-bottom:10px;">
                                <a href="https://www.instagram.com/partidoecologistaverde" target="_blank"><img src="http://cop25ciudadana.cl/mail-img/ico-instagram.png" alt="Instagram Partido Ecologista Verde" width="50" height="50" style="display:block; margin:0 auto; border:0px" border="0"></a>
                            </td>

                        </tr>

                    </tbody></table>

                </td>

            </tr>

        </tbody></table>   
    </body>
</html>
