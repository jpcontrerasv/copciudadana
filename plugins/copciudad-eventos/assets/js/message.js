jQuery(document).ready(function($) {
    var messageEvent = new Object();
    messageEvent.form = $('form[rel="message"]');
    messageEvent.formNotice = $('form[rel="message"] [rel="notice"]');

    messageEvent.form.validate = function() {
        var validate = true;
        if (messageEvent.form[0].checkValidity() == false) {
                $('input:invalid, select:invalid, textarea:invalid').addClass('error');
                validate = false;
        }
        messageEvent.invalidScroll();
        return validate;
    }


    messageEvent.invalidScroll = function () {
        var topError = $('input:invalid, select:invalid, textarea:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    messageEvent.disableForms = function() {
        messageEvent.form.find('input, select').prop('disabled', true);
        messageEvent.form.css({ opacity: 0.5 });
    }
    
    messageEvent.enableForms = function() {
        messageEvent.form.find('input, select').prop('disabled', false);
        messageEvent.form.css({ opacity: 1 });
    }

    
    messageEvent.noticeWarning = function(message) {
        messageEvent.formNotice.addClass('alert-warning');
        messageEvent.formNotice.text(message);
        messageEvent.formNotice.show();
        messageEvent.noticeScroll();
        setTimeout(function() {
            messageEvent.formNotice.hide();
        }, 5000);
    }

    messageEvent.noticeSuccess = function(message) {
     
        messageEvent.formNotice.addClass('alert-success');
        messageEvent.formNotice.text(message);
        messageEvent.formNotice.show();
        messageEvent.noticeScroll();
        setTimeout(function() {
            messageEvent.formNotice.hide();
        }, 5000);

    }
    
    messageEvent.noticeScroll = function() {
        var topError =  $('form[rel="message"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    messageEvent.form.find('input[type=submit]').click(function(e) {
        e.preventDefault();
        if(messageEvent.form.validate()) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: messageEvent.form.serialize()+'&action=ajax_message_event',
                beforeSend: function() {
                    messageEvent.disableForms();
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    if(response.status === 'success') {
                        messageEvent.noticeSuccess(message);
                    } else {
                        messageEvent.noticeWarning(message);
                    }
                    messageEvent.enableForms();
                }
            })
        }
    });
});