jQuery(document).ready(function($) {
    var inscription = new Object();
    inscription.form = $('form[rel="inscription"]');
    inscription.invalidInput = $('input:invalid, select:invalid');
    inscription.equalsInput =  $('input[equals]');
    inscription.formNotice = $('form[rel="inscription"] [rel="notice"]');

    inscription.form.validate = function() {
        var validate = true;
        if (inscription.form[0].checkValidity() == false) {
                $('input:invalid, select:invalid').addClass('error');
                validate = false;
        }
        inscription.equalsInput.map(function(index, element) {
                var name = inscription.form.find(element).attr('equals');
                var value =  inscription.form.find(element).val();
                var equals = inscription.form.find(`[name=${name}]`).val();
                if(value !== equals) {
                    inscription.form.find(element).removeClass('error');
                    validate = false;
                }
        });
        inscription.invalidScroll();
        return validate;
    }
    

    inscription.disableForms = function() {
    
        inscription.form.find('input, select').prop('disabled', true);
        inscription.form.css({ opacity: 0.5 });
    }
    
    inscription.enableForms = function() {
        inscription.form.find('input, select').prop('disabled', false);
        inscription.form.css({ opacity: 1 });
    }

    inscription.noticeWarning = function(message) {
        inscription.formNotice.addClass('alert-warning');
        inscription.formNotice.text(message);
        inscription.formNotice.show();
        inscription.noticeScroll();
        setTimeout(function() {
            inscription.formNotice.hide();
        }, 5000);
    }

    inscription.noticeSuccess = function(message) {
     
        inscription.formNotice.addClass('alert-success');
        inscription.formNotice.text(message);
        inscription.formNotice.show();
        inscription.noticeScroll();
        setTimeout(function() {
            inscription.formNotice.hide();
        }, 5000);

    }

    inscription.invalidScroll = function () {
        var topError = $('input:invalid, select:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    inscription.noticeScroll = function() {
        var topError =  $('form[rel="inscription"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }
   
   
    inscription.form.find('input[type=submit]').click(function(event) {
        event.preventDefault();
        if(inscription.form.validate()) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: inscription.form.serialize()+'&action=ajax_inscription',
                beforeSend: function() {
                    inscription.disableForms();
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    if(response.status === 'success') {
                        var url = response.url;
                        inscription.noticeSuccess(message);
                        inscription.form[0].reset();
                        setTimeout(function() {
                            window.location.href = url;
                        },500);
                    } else {
                        inscription.noticeWarning(message);
                        inscription.enableForms();
                    }
                },
                error: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    inscription.noticeWarning(message);
                    inscription.enableForms();
                }
            })
        }
    });

    

    inscription.invalidInput.on('input',function() {
        var value = $(this).val();
        if(value){
            $(this).removeClass('error');
        }
    });
});