jQuery(document).ready(function($) {
    var act = new Object();
    act.form = $('form[rel="act"]');
    act.formNotice = $('form[rel="act"] [rel="notice"]');

    act.form.validate = function() {
        var validate = true;
        if (act.form[0].checkValidity() == false) {
                $('input:invalid, select:invalid').addClass('error');
                validate = false;
        }
        act.invalidScroll();
        return validate;
    }


    act.invalidScroll = function () {
        var topError = $('input:invalid, select:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    act.disableForms = function() {
        act.form.find('input, select').prop('disabled', true);
        act.form.css({ opacity: 0.5 });
    }
    
    act.enableForms = function() {
        act.form.find('input, select').prop('disabled', false);
        act.form.css({ opacity: 1 });
    }

    
    act.noticeWarning = function(message) {
        act.formNotice.addClass('alert-warning');
        act.formNotice.text(message);
        act.formNotice.show();
        act.noticeScroll();
        setTimeout(function() {
            act.formNotice.hide();
        }, 5000);
    }

    act.noticeSuccess = function(message) {
     
        act.formNotice.addClass('alert-success');
        act.formNotice.text(message);
        act.formNotice.show();
        act.noticeScroll();
        setTimeout(function() {
            act.formNotice.hide();
        }, 5000);

    }
    
    act.noticeScroll = function() {
        var topError =  $('form[rel="act"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    act.form.find('input[type=submit]').click(function(e) {
        e.preventDefault();
        if(act.form.validate()) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: act.form.serialize()+'&action=ajax_update_act',
                beforeSend: function() {
                    act.disableForms();
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    if(response.status === 'success') {
                        act.noticeSuccess(message);
                    } else {
                        act.noticeWarning(message);
                    }
                    act.enableForms();
                }
            })
        }
    });
});