jQuery(document).ready(function($) {
    var events = new Object();
    
    events.participants = $('#participantes');
    events.form = $('form[rel="event"]');
    events.formNotice = $('form[rel="event"] [rel="notice"]');
    events.modalDelete = $("#modal-eliminar");
    events.modalParticipar = $("#modal-participar");
    events.joinNotice = $('[rel="notice-join"]');
    events.modalDejar = $('#modal-no-participar');
    events.quitNotice = $('[rel="notice-join"]');


    events.form.validate = function() {
        var validate = true;
        if (events.form[0].checkValidity() == false) {
                $('input:invalid, select:invalid').addClass('error');
                validate = false;
        }
        events.invalidScroll();
        return validate;
    }

    events.invalidScroll = function () {
        var topError = $('input:invalid, select:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    events.noticeWarning = function(message) {
        events.formNotice.addClass('alert-warning');
        events.formNotice.text(message);
        events.formNotice.show();
        events.noticeScroll();
        setTimeout(function() {
            events.formNotice.hide();
        }, 5000);
    }

    events.noticeSuccess = function(message) {
     
        events.formNotice.addClass('alert-success');
        events.formNotice.text(message);
        events.formNotice.show();
        events.noticeScroll();
        setTimeout(function() {
            events.formNotice.hide();
        }, 5000);

    }


    events.noticeJoinWarning = function(message) {
        events.joinNotice.addClass('alert-warning');
        events.joinNotice.text(message);
        events.joinNotice.show();
        setTimeout(function() {
            events.joinNotice.hide();
        }, 5000);
    }

    events.noticeJoinSuccess = function(message) {
        events.joinNotice.addClass('alert-success');
        events.joinNotice.text(message);
        events.joinNotice.show();
        setTimeout(function() {
            events.joinNotice.hide();
        }, 5000);

    }


    events.noticeQuitWarning = function(message) {
        events.quitNotice.addClass('alert-warning');
        events.quitNotice.text(message);
        events.quitNotice.show();
        setTimeout(function() {
            events.quitNotice.hide();
        }, 5000);
    }

    events.noticeQuitSuccess = function(message) {
        events.quitNotice.addClass('alert-success');
        events.quitNotice.text(message);
        events.quitNotice.show();
        setTimeout(function() {
            events.quitNotice.hide();
        }, 5000);

    }

    
    events.noticeScroll = function() {
        var topError =  $('form[rel="event"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    events.disableForms = function() {
        events.form.find('input, select').prop('disabled', true);
        events.form.css({ opacity: 0.5 });
    }
    
    events.enableForms = function() {
        events.form.find('input, select').prop('disabled', false);
        events.form.css({ opacity: 1 });
    }

   
    events.form.find('input[type=submit]').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: events.form.serialize()+'&action=ajax_update_event',
            beforeSend: function() {
                events.disableForms();
            },
            success: function( data ) {
                var response = JSON.parse(data);
                var message = response.message;
                if(response.status === 'success') {
                    events.noticeSuccess(message);
                } else {
                    events.noticeWarning(message);
                }
                events.enableForms();
            }
        })
    });

    events.participants.on("click",'[data-action="delete"]', function() {
        var participantId = $(this).data('id');
        var eventId = $(this).data('event');
        var row = $(this).closest('tr');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'action=ajax_delete_participant&user='+participantId+'&event='+eventId,
            beforeSend: function() {
                events.participants .css({opacity: '0.5' });
            },
            success: function( data ) {
                var response = JSON.parse(data);
                if(response.status == 'success') {
                    row.remove();
                }
                events.participants .css({ opacity: '1' });
            }
        })
    });

    events.modalDelete.on("click", '[data-action="delete"]', function() {
        var postId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'action=ajax_delete_event&event='+postId,
            beforeSend: function() {
                events.modalDelete.css({ opacity: '0.5' });
            },
            success: function( data ) {
                var response = JSON.parse(data);
                if(response.status == 'success') {
                    var url = response.url;
                    setTimeout(function() {
                        window.location.href = url;
                    },500);
                }
            }
        })
    });

    events.modalParticipar.on("click", '[data-action="join"]', function() {
        var postId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'action=ajax_join_event&event='+postId,
            success: function( data ) {
                var response = JSON.parse(data);
                var message = response.message;
                if(response.status == 'success') {
                    var url = response.url;
                    events.noticeJoinSuccess(message);
                    setTimeout(function() {
                        window.location.href = url;
                    },500);
                } else {
                    events.noticeJoinWarning(message);
                }
            }
        })
    });

    events.modalDejar.on("click", '[data-action="quit"]', function() {
        var postId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'action=ajax_quit_event&event='+postId,
            success: function( data ) {
                var response = JSON.parse(data);
                var message = response.message;
                if(response.status == 'success') {
                    var url = response.url;
                    events.noticeQuitSuccess(message);
                    setTimeout(function() {
                        window.location.href = url;
                    },500);
                } else {
                    events.noticeQuitWarning(message);
                }
            }
        })
    });
});