jQuery(document).ready(function($) {
    var profile = new Object();
    profile.form = $('form[rel="profile"]');
    profile.allInput = $('input');
    profile.equalsInput =  $('input[equals]');
    profile.formNotice = $('form[rel="profile"] [rel="notice"]');
    profile.templateParticipateEvents = $('#template-profile-participate');
    profile.renderParticipateEvents = $('#render-profile-participate');
    profile.templateMyEvents = $('#template-profile-events');
    profile.renderMyEvents = $('#render-profile-events');
    

    profile.form.validate = function() {
        var validate = true;
        if (profile.form[0].checkValidity() == false) {
                $('input:invalid').attr('class', 'error');
                validate = false;
        }
        profile.equalsInput.map(function(index, element) {
                var name = profile.form.find(element).attr('equals');
                var value =  profile.form.find(element).val();
                var equals = profile.form.find(`[name=${name}]`).val();
                if(value !== equals) {
                    profile.form.find(element).attr('class', 'error');
                    validate = false;
                }
        });
        profile.invalidScroll();
        return validate;
    }
    
    profile.disableForms = function() {
    
        profile.form.find('input, select').prop('disabled', true);
        profile.form.css({ opacity: 0.5 });
    }
    
    profile.enableForms = function() {
        profile.form.find('input, select').prop('disabled', false);
        profile.form.css({ opacity: 1 });
    }

    profile.noticeWarning = function(message) {
        profile.formNotice.addClass('alert-warning');
        profile.formNotice.text(message);
        profile.formNotice.show();
        profile.noticeScroll();
        setTimeout(function() {
            profile.formNotice.hide();
        }, 5000);
    }

    profile.noticeSuccess = function(message) {
     
        profile.formNotice.addClass('alert-success');
        profile.formNotice.text(message);
        profile.formNotice.show();
        profile.noticeScroll();
        setTimeout(function() {
            profile.formNotice.hide();
        }, 5000);

    }

    profile.noticeScroll = function() {
        var topError =  $('form[rel="profile"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    profile.invalidScroll = function () {
        var topError = $('input:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }
   
    profile.templateRender = function( templateid, data ){
        return document.getElementById( templateid ).innerHTML
          .replace(
            /{{(\w*)}}/g, // or /{(\w*)}/g for "{this} instead of %this%"
            function( m, key ){
              return data.hasOwnProperty( key ) ? data[ key ] : "";
            }
          );
    }

    profile.form.find('input[type=submit]').click(function(event) {
        event.preventDefault();
        if(profile.form.validate()) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: profile.form.serialize()+'&action=ajax_profile',
                beforeSend: function() {
                    profile.disableForms();
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    if(response.status === 'success') {
                        profile.noticeSuccess(message);
                    } else {
                        profile.noticeWarning(message);
                    }
                    profile.enableForms();
                },
                error: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    profile.noticeWarning(message);
                    profile.enableForms();
                }
            })
        }
    });
    

    profile.allInput.on('input',function() {
        var value = $(this).val();
        if(value){
            $(this).removeClass('error');
        }
    });

    profile.loadParticipateEvent = (function() {
        var html = '';
        if(profile.templateParticipateEvents.length !== 0) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: 'action=ajax_profile_list_participate_event',
                beforeSend: function() {
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    response.map(function(item) {
                        html = profile.templateRender("template-profile-participate", {
                            "id": item.id,
                            "title": item.title,
                            "date": item.date,
                            "address": item.address,
                            "url": item.url
                        }) + html;
                    })
                    profile.renderParticipateEvents .html(html);
                },
            })
        }
    }());


    profile.loadMyEvent = (function() {
        var html = '';
        if(profile.templateMyEvents.length !== 0) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: 'action=ajax_profile_my_events',
                beforeSend: function() {
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    response.map(function(item) {
                        html = profile.templateRender("template-profile-events", {
                            "id": item.id,
                            "title": item.title,
                            "url": item.url
                        }) + html;
                    })
                    profile.renderMyEvents .html(html);
                },
            })
        }
    }());
})