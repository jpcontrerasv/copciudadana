jQuery(document).ready(function($) {
    var register = new Object();
    register.form = $('form[rel="register"]');
    register.invalidInput = $('input:invalid');
    register.equalsInput =  $('input[equals]');
    register.formNotice = $('form[rel="register"] [rel="notice"]');

    register.form.validate = function() {
        var validate = true;
        if (register.form[0].checkValidity() == false) {
                $('input:invalid').attr('class', 'error');
                validate = false;
        }
        
        register.equalsInput.map(function(index, element) {
                var name = register.form.find(element).attr('equals');
                var value = register.form.find(element).val();
                var equals = register.form.find(`[name=${name}]`).val();
                if(value !== equals) {
                    register.form.find(element).attr('class', 'error');
                    validate = false;
                }
        });
        register.invalidScroll();
        return validate;
    }
    
    register.disableForms = function() {
    
        register.form.find('input, select').prop('disabled', true);
        register.form.css({ opacity: 0.5 });
    }
    
    register.enableForms = function() {
        register.form.find('input, select').prop('disabled', false);
        register.form.css({ opacity: 1 });
    }

    register.noticeWarning = function(message) {
        register.formNotice.addClass('alert-warning');
        register.formNotice.text(message);
        register.formNotice.show();
        register.noticeScroll();
        setTimeout(function() {
            register.formNotice.hide();
        }, 5000);
    }

    register.noticeSuccess = function(message) {
     
        register.formNotice.addClass('alert-success');
        register.formNotice.text(message);
        register.formNotice.show();
        register.noticeScroll();
        setTimeout(function() {
            register.formNotice.hide();
        }, 5000);

    }

    register.noticeScroll = function() {
        var topError =  $('form[rel="register"] [rel="notice"]');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }

    register.invalidScroll = function () {
        var topError = $('input:invalid');
        var head = $('#header');
        if(topError.length !== 0) {
            topError.top = parseInt(topError.offset().top);
            head.top = parseInt(head.outerHeight() + 100);
            $(window).scrollTop(topError.top - head.top);
        }
    }
   
    register.form.find('input[type=submit]').click(function(event) {
        event.preventDefault();
        if(register.form.validate()) {
            $.ajax({
                url: baseURL+"/wp-admin/admin-ajax.php",
                type: 'POST',
                data: register.form.serialize()+'&action=ajax_register',
                beforeSend: function() {
                    register.disableForms();
                },
                success: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    if(response.status === 'success') {
                        var url = response.url;
                        register.noticeSuccess(message);
                        register.form[0].reset();
                        setTimeout(function() {
                            window.location.href = url;
                        },500);
                    } else {
                        register.noticeWarning(message);
                        register.enableForms();
                    }
                },
                error: function( data ) {
                    var response = JSON.parse(data);
                    var message = response.message;
                    register.noticeWarning(message);
                    register.enableForms();
                }
            })
        }
    });
    

    register.invalidInput.on('input',function() {
        var value = $(this).val();
        if(value){
            $(this).removeClass('error');
        }
    });

})