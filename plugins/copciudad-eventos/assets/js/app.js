jQuery(document).ready(function($){
    var app = new Object();
    app.login = $("#modal-ingresa [data-action='login']");
    app.formLogin = $('form[rel="login"]');
    app.formLoginNotice = $('form[rel="login"] [rel="notice-login"]');
    app.tableManager = $('table[rel="admin-manager"]');
    app.tableEvent = $('table[rel="table-event"]');
    
    app.tableManager.bootstrapTable();
    app.tableEvent.bootstrapTable();
    
    app.disableLoginForms = function() {
        app.formLogin.find('input, select').prop('disabled', true);
        app.formLogin.css({ opacity: 0.5 });
    }
    
    app.enableLoginForms = function() {
        app.formLogin.find('input, select').prop('disabled', false);
        app.formLogin.css({ opacity: 1 });
    }

    app.noticeLoginWarning = function(message) {
        app.formLoginNotice.addClass('alert-warning');
        app.formLoginNotice.text(message);
        app.formLoginNotice.show();
        setTimeout(function() {
            app.formLoginNotice.hide();
        }, 5000);
    }

    app.noticeLoginSuccess = function(message) {
     
        app.formLoginNotice.addClass('alert-success');
        app.formLoginNotice.text(message);
        app.formLoginNotice.show();
        setTimeout(function() {
            app.formLoginNotice.hide();
        }, 5000);

    }

    app.login.click(function() {
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data:  app.formLogin.serialize()+'&action=ajax_login',
            beforeSend: function() {
                app.disableLoginForms();
            },
            success: function( data ) {
                var response = JSON.parse(data);
               if(response.status === 'success'){
                    var url = response.url;
                    window.location.href = url;
               } else {
                    var message = response.message;
                    app.noticeLoginWarning(message);
               }
               app.enableLoginForms();
            },
        })
    });

    app.tableManager.on('click', '[data-action="reject"]', function() {
        var eventId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'event='+eventId+'&action=ajax_reject_event',
            beforeSend: function() {
                app.tableManager.css({opacity: '0.5'});
            },
            success: function( data ) {
                app.tableManager.css({opacity: '1'});
                app.tableManager.bootstrapTable('refresh');
            },
        });
    });

    app.tableManager.on('click', '[data-action="delete"]', function() {
        var eventId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'event='+eventId+'&action=ajax_delete_event',
            beforeSend: function() {
                app.tableManager.css({opacity: '0.5'});
            },
            success: function( data ) {
                app.tableManager.css({opacity: '1'});
                app.tableManager.bootstrapTable('refresh');
            },
        })
    });

    app.tableManager.on('click', '[data-action="approve"]',function() {
        var eventId = $(this).data('id');
        $.ajax({
            url: baseURL+"/wp-admin/admin-ajax.php",
            type: 'POST',
            data: 'event='+eventId+'&action=ajax_approve_event',
            beforeSend: function() {
                app.tableManager.css({opacity: '0.5'});
            },
            success: function( data ) {
               app.tableManager.css({opacity: '1'});
               app.tableManager.bootstrapTable('refresh');
            },
        });
    })
})